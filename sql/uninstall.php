<?php

/**
 *  2020-2021 FARMALISTO
 *
 *  @author    Jhonny Romero <jromeroarenales@gmail.com>
 *  @copyright 2017-2018 FARMALISTO
 *  @license   https://www.farmalisto.com.mx/ - prestashop module orbisfarma
 */
$sql = array();
$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'orbisfarma_customer_card`';
$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'orbisfarma_transaction_init`';
$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'orbisfarma_transaction_quote`';
$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'orbisfarma_transaction_sale`';
$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'orbisfarma_sale_reverse`';
$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'orbisfarma_service`';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}

<?php

/**
 *  2020-2021 FARMALISTO
 *
 *  @author    Jhonny Romero <jromeroarenales@gmail.com>
 *  @copyright 2017-2018 FARMALISTO
 *  @license   https://www.farmalisto.com.mx/ - prestashop module orbisfarma
 */
$sql = array();

$sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "orbisfarma_customer_card` (
    `id_customer`  int(11) UNSIGNED NOT NULL ,
    `id_orbisfarma_service`  int(11) NOT NULL ,
    `number`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
    `date_add`  datetime NULL DEFAULT CURRENT_TIMESTAMP
)
ENGINE=" . _MYSQL_ENGINE_ . "
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;";

$sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "orbisfarma_transaction_init` (
     `id_orbisfarma_service`  int(11) UNSIGNED NOT NULL ,
    `id_cart`  int(11) UNSIGNED NOT NULL ,
    `id_order`  int(11) NULL DEFAULT 0,
    `cardnumber`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' ,
    `transactionid`  varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' ,
    `saleauthnumber`  varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' ,
    `errorid`  int(11) NULL DEFAULT 0,
    `sended`  tinyint(1) NOT NULL DEFAULT 0 ,
    `transactiondate`  datetime NULL DEFAULT NULL ,
    `date_add`  datetime NULL DEFAULT current_timestamp() ,
    `date_upd`  datetime NULL DEFAULT current_timestamp() ON UPDATE CURRENT_TIMESTAMP ,
    PRIMARY KEY (`id_cart`, `id_orbisfarma_service`)
)
ENGINE=" . _MYSQL_ENGINE_ . "
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;";

$sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "orbisfarma_transaction_quote` (
    `id_orbisfarma_service`  int(11) UNSIGNED NOT NULL ,
    `id_cart`  int(11) UNSIGNED NOT NULL ,
    `id_product`  int(11) NOT NULL ,
    `reference`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' ,
    `discount_quantity`  int(11) NULL DEFAULT NULL,
    `discount`  decimal(6,4) NULL DEFAULT NULL,
    `gift_quantity`  int(11) NULL DEFAULT NULL,
    `transactionid`  varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' ,
    `transactiondate`  datetime NULL DEFAULT NULL,
    `date_add`  datetime NULL DEFAULT current_timestamp() ,
    PRIMARY KEY (`id_cart`, `id_product`, `id_orbisfarma_service`)
)
ENGINE=" . _MYSQL_ENGINE_ . "
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;";

$sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "orbisfarma_transaction_sale` (
    `id_transaction_sale`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `id_order`  int(11) NULL DEFAULT NULL ,
    `id_cart`  int(11) UNSIGNED NOT NULL ,
    `transactionid`  varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
    `saleauthnumber`  varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
    `cardnumber`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
    `cardbalance`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
    `invoicemessage`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
    `errorid`  int(11) NULL DEFAULT NULL ,
    `id_product`  int(11) NOT NULL ,
    `reference`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
    `discount_quantity`  int(11) NULL DEFAULT NULL ,
    `gift_quantity`  int(11) NULL DEFAULT NULL ,
    `discount`  decimal(6,4) NULL DEFAULT NULL ,
    `points`  int(11) NULL DEFAULT NULL ,
    `transactiondate`  datetime NULL DEFAULT NULL ,
    `date_add`  datetime NULL DEFAULT CURRENT_TIMESTAMP ,
    PRIMARY KEY (`id_transaction_sale`)
)
ENGINE=" . _MYSQL_ENGINE_ . "
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;";

$sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "orbisfarma_sale_reverse` (
    `id_sale_reverse`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `transactionid`  varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
    `saleauthnumber`  varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
    `cardnumber`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
    `errorid`  int(11) NULL DEFAULT NULL ,
    `message`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
    `transactiondate`  datetime NULL DEFAULT NULL ,
    `date_add`  datetime NULL DEFAULT CURRENT_TIMESTAMP ,
    PRIMARY KEY (`id_sale_reverse`)
)
ENGINE=" . _MYSQL_ENGINE_ . "
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;";

$sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "orbisfarma_service` (
    `id_orbisfarma_service`  int(11) NOT NULL AUTO_INCREMENT ,
    `name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' ,
    `description`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' ,
    `key`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' ,
    `key_sandbox`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' ,
    `wsdl`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' ,
    `wsdl_sandbox`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' ,
    `prod_active`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
    `active`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
    `date_add`  datetime NULL DEFAULT current_timestamp() ,
    `date_upd`  datetime NULL DEFAULT current_timestamp() ON UPDATE CURRENT_TIMESTAMP ,
PRIMARY KEY (`id_orbisfarma_service`)
)
ENGINE=" . _MYSQL_ENGINE_ . "
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;";

$sql[] = "INSERT INTO `ps_orbisfarma_service` (`name`, `description`, `key`, `key_sandbox`, `wsdl`,`wsdl_sandbox`, `prod_active`, `active`) VALUES ('nuevoyo', 'Nuevo Yo','5483AE69B760ABDF512C75C58A9219F4','5483AE69B760ABDF512C75C58A9219F4', 'https://orbisws01.orbisfarma.com.mx/transaccion.asmx?wsdl', 'http://orbisws00.orbisfarma.com.mx/transaccion.asmx?wsdl', '0', '0');";
$sql[] = "INSERT INTO `ps_orbisfarma_service` (`name`, `description`, `key`, `key_sandbox`, `wsdl`,`wsdl_sandbox`, `prod_active`, `active`) VALUES ('enlacevital', 'Enlace Vital','BBD796A1C5931A08505F79D8E6DCB65C','BBD796A1C5931A08505F79D8E6DCB65C', 'https://orbiswsapp02.orbisfarma.com.mx/Transaccion.asmx?wsdl', 'https://orbiswsapp02.orbisfarma.com.mx/Transaccion.asmx?wsdl', '0', '0');";
$sql[] = "INSERT INTO `ps_orbisfarma_service` (`name`, `description`, `key`, `key_sandbox`, `wsdl`,`wsdl_sandbox`, `prod_active`, `active`) VALUES ('abracelavida', 'Abrace la Vida','5483AE69B760ABDF512C75C58A9219F4','5483AE69B760ABDF512C75C58A9219F4', 'https://orbisws00.orbisfarma.com.mx/Transaccion.asmx?wsdl', 'https://orbisws01.orbisfarma.com.mx/Transaccion.asmx?wsdl', '0', '0');";

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}

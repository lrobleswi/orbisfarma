/**
 *  2019-2020 FARMALISTO
 *
 *  @author    Jhonny Romero <jromeroarenales@gmail.com>
 *  @copyright 2017-2020 FARMALISTO
 *  @license   https://www.farmalisto.com.mx/ - prestashop module orbisfarma
 */

$(document).ready(function () {
    var form = $('form#add-card');

    prestashop.blockcart = prestashop.blockcart || {};
    var showModal = prestashop.blockcart.showModal || function (modal) {
        var $body = $('body');
        $body.append(modal);
        $body.one('click', '#blockcart-modal', function (event) {
            if (event.target.id === 'blockcart-modal') {
                $(event.target).remove();
            }
        });
    };

    form.submit(function (e) {
        e.preventDefault();
        var id = $('#id_service').find(":selected").val();
        var plan = $('#id_service').find(":selected").data('plan');
        if (id == null || id == '' || id == undefined) {
            var id = 3;
        }
        if (plan == null || plan == '' || plan == undefined) {
            var plan = $('input:hidden[name=plan]').val();
        }
        $.ajax({
            type: 'POST',
            headers: {"cache-control": "no-cache"},
            url: $('input:hidden[name=url_add_card]').val(),
            async: true,
            dataType: 'json',
            data: {
                ajax: true,
                action: 'addCard',
                token: $('input:hidden[name=token]').val(),
                card_number: $('input:text[name=card_number]').val(),
                plan: plan,
                id_service: id,
                url: $('input:hidden[name=request_uri]').val(),
                redirect: $('input:hidden[name=redirect]').val(),
            },
            success: function (resp)
            {
                var time;
                if (resp.save) {
                    time = 2000;
                    $("#mazsalud-card").replaceWith();
                } else {
                    time = 4000;
                }
                if (resp.message) {
                    showModal(resp.message);
                    setTimeout(function () {
                        $('#blockcart-modal').modal('hide');
                        if (resp.save === false && resp.logged === false) {
                            window.location.href = resp.url;
                        }
                        if (resp.save === true && resp.redirect === true) {
                            window.location.href = resp.url;
                        }
                    }, time);
                }
            }
        });
        return false;
    });
    $('#id_service').change(function () {
        console.log('data');
        var id_orbisfarma_service = $('#id_service').find(":selected").val();
        if (id_orbisfarma_service)
            $('#add-card-service').css('display', 'flex');
        else
            $('#add-card-service').css('display', 'none');
        return this;
    });

});

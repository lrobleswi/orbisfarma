
$(document).ready(function () {
    $('#orbisfarma-card-form input').bind('input propertychange', function () {

        if ($(this).val().length != 0) {
            document.querySelector('#orbisfarma-cart-save').textContent = 'Guardar';
        } else {
            document.querySelector('#orbisfarma-cart-save').textContent = 'Borrar Tarjeta';
        }
    });
});

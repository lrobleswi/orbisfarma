{**
 *  2019-2020 FARMALISTO
 *
 *  @author    Jhonny Romero <jromeroarenales@gmail.com>
 *  @copyright 2017-2020 FARMALISTO
 *  @license   https://www.farmalisto.com.mx/ - prestashop module orbisfarma
 *}

 <style>
    .text{
        font-family: Open Sans;

    }
    .label-lealtad{
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 16px;

        text-align: center;
        color: #3A9B37;
    }

    #id_service{
        height: 56px !important;
        border-radius: 16px !important;
        margin: auto !important;
        padding: 10px !important;
        font-size: 14px !important;
        line-height: 24px !important;
        color: #43475A !important;
        margin-top: 10px !important;
        font-weight: 100 !important;
        appearance: auto !important;
    }

    .flex-container{
        margin-bottom: 10px;
    }

    .add-card{
            color: gray !important;
            background-color: #fdfdfd !important;
            width: 118px;
            height: 24px;
            margin: auto;
    border: none;
    text-transform: initial;
    }

    .add-card-service{
        padding: 0 20px;
    }

    .add-card-service input{
           font-family: Open Sans;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 16px;
    text-align: center;
    color: #A6A6A6;
    height: 32px;
    }

    .leyen-data{
        font-family: Open Sans;
        font-style: normal;
        font-weight: normal;
        font-size: 9px;
        line-height: 16px;
        color: #FF565E;
        text-align: center;
        padding-top: 5px;
        margin-bottom: 5px;
    }

    .caja {
   margin:20px auto 40px auto;	
   border:1px solid #d9d9d9;
   height:30px;
   overflow: hidden;
   width: 230px;
   position:relative;
}
select {
   background: transparent;
   border: none;
   font-size: 14px;
   height: 30px;
   padding: 5px;
   width: 250px;
}
select:focus{ outline: none;}
.id_service::after{
	pointer-events: none;

}
.caja::after{
	content:"\025be";
	display:table-cell;
	padding-top:7px;
	text-align:center;
	width:30px;
	height:30px;
	background-color:#d9d9d9;
	position:absolute;
	top:0;
	right:0px;	
	pointer-events: none;
}

 </style>
<div class="collapse-cart" id="mazsalud-card" style="padding: 1.6rem; vertical-align: middle;">


   <form id="add-card" data-link-action="add-card" method="post">
       <input type="hidden" name="token" value="{$static_token}">
       <input type="hidden" name="url_add_card" value="{$url_add_card}">
       <input type="hidden" name="request_uri" value="{$request_uri}">
       <input type="hidden" name="redirect" value="1">
       <div class="flex-container-custom">
           <div class="w-100">
                <div style="text-align: center;">
                    <label class="label-lealtad">{l s="¿Tienes tarjeta de algún programa de lealtad?"}</label>
                </div>
               <div class="relative icon-true">        
                   <select class="form-control" name="id_service" required="" id="id_service" style="padding: 10px 15px !important;">
                       <option value="">{l s='Selecciona tu programa de lealtad' mod='orbisfarma'}</option>
                       {foreach from=$attachment_plans item=plan}
                        <option data-plan="{$plan['module']}" value="{if isset($plan['id_orbisfarma_service'])}{$plan['id_orbisfarma_service']}{elseif isset($plan['id_lms_service'])}{$plan['id_lms_service']}{/if}">{$plan['description']}</option>
                       {/foreach}
                   </select>
               </div>        
           </div>
       </div>
        <p class="leyen-data">Para agregar tu tarjeta, debes iniciar sesión en Farmalisto.</p>
       <div id="add-card-service" class="flex-container relative" style="display: none;     padding: 0 20PX;">
           <input class="promo-input-custom"  type="text" name="card_number" placeholder="{l s='INGRESA TU TARJETA' mod='orbisfarma'}">
           <button type="submit" class="btn add-card">{l s='Activar' d='Shop.Theme.Actions'}</button>
       </div>
   </form>
{* <div class="caja">
  <select>
    <option>La primera opción</option>
    <option>La segunda opción</option>
    <option>La tercera opción</option>
  </select>
</div> *}

   {if isset($errors)}
   <div class="alert alert-danger js-error" role="alert">
       <i class="material-icons">&#xE001;</i><span class="ml-1 js-error-text"></span>
   </div>
   {/if}
</div>
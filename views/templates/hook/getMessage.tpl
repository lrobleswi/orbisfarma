{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

 <style>
 .content-card{

 background: #DAE8DF;
border-radius: 8px;
    margin: auto;
    padding: auto;
    width: 140px;
    height: 140px;
    display: flex;
    justify-items: center;
    align-items: center;
    padding-left: 10px;
 }
 .auto{
   margin: auto;
 }

 .modal-content {
    border-radius: 15px;
    width: 751px;
    height: 362px;
    padding: 30px;
    margin: auto;
}
</style>
<div id="blockcart-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" clip-rule="evenodd" d="M9.03431 10.1657L0 1.13137L1.13137 0L10.1657 9.03431L19.2 0L20.3314 1.13137L11.2971 10.1657L20.3314 19.2L19.2 20.3314L10.1657 11.2971L1.13137 20.3314L0 19.2L9.03431 10.1657Z" fill="#3A9B37"/>
          </svg>
          Salir
        </button>
      </div>
      <div class="modal-body " style="text-align: center;">
        <div class="content-card auto">
          <svg width="120" height="102" viewBox="0 0 120 102" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M111 42H33C28.038 42 24 46.038 24 51V93C24 97.962 28.038 102 33 102H111C115.962 102 120 97.962 120 93V51C120 46.038 115.962 42 111 42ZM114 93C114 94.656 112.656 96 111 96H33C31.344 96 30 94.656 30 93V51C30 49.344 31.344 48 33 48H111C112.656 48 114 49.344 114 51V93ZM102 75C100.398 75 98.868 75.471 97.512 76.257C96.18 75.48 94.65 75 93 75C88.038 75 84 79.038 84 84C84 88.962 88.038 93 93 93C94.65 93 96.18 92.52 97.512 91.743C98.868 92.529 100.398 93 102 93C106.962 93 111 88.962 111 84C111 79.038 106.962 75 102 75ZM93 87C91.344 87 90 85.656 90 84C90 82.344 91.344 81 93 81C94.656 81 96 82.344 96 84C96 85.656 94.656 87 93 87ZM102 87C101.82 87 101.658 86.919 101.481 86.889C101.793 85.977 102 85.017 102 84C102 82.983 101.793 82.023 101.481 81.111C101.658 81.081 101.82 81 102 81C103.656 81 105 82.344 105 84C105 85.656 103.656 87 102 87ZM6 51V21H90V36H96V9C96 4.038 91.962 0 87 0H9C4.038 0 0 4.038 0 9V51C0 55.962 4.038 60 9 60H18V54H9C7.344 54 6 52.656 6 51ZM9 6H87C88.656 6 90 7.344 90 9V15H6V9C6 7.344 7.344 6 9 6Z" fill="#3A9B37"/>
          </svg>
        </div>

        <h4 class="modal-title h6 flex-container align-items-center auto" id="myModalLabel">¡{$message}</h4>     
      </div>
        <button type="button" class="btn btn-primary"  data-dismiss="modal">Aceptar</button>
    </div>
  </div>
</div>

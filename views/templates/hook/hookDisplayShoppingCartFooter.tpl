{*
 *  FARMALISTO
 *
 *  @author    Jhonny Romero <jromeroarenales@gmail.com>
 *  @copyright 2017-2021 FARMALISTO
 *  @license   https://www.farmalisto.com.mx/ - prestashop module orbisfarma
*}
 <div id="footer-orbisfarma" data-footer-orbisfarma ="{$url_footer_orbisfarma}" style="padding: 10px;">
    {if $title}
    <h3 style="margin-top: 10px; margin-bottom: 20px;">{l s="Producto(s) GRATIS en tu pedido"}</h3>

    {/if}
 {foreach $plans as $plan}
    {if isset($form_confirmations_{$plan.name}) || isset($form_errors_{$plan.name}) && $plan.active} 
        <div style="padding-bottom: 5px;">
            <img src="../modules/orbisfarma/views/img/front/{$plan.name}.svg" style="width:20%; " />
        </div>
        <div class="warning">

            {if isset($form_confirmations_{$plan.name})}  
                {foreach from=$form_confirmations_{$plan.name} item=confirmation}
                    {* <img src="../modules/orbisfarma/views/img/front/megaphone.png" alt="descuento" />&nbsp; - <b>{$confirmation}</b><br>   *}
                    <span><b>{$confirmation}</b></span><br>  
                {/foreach}
            {/if} 
            {if isset($form_errors_{$plan.name})} 
                {foreach from=$form_errors_{$plan.name} item=error}
                    {* <img src="../modules/orbisfarma/views/img/front/error.png" alt="error" />&nbsp;- <b>{$error}</b><br> *}
                    <span><b>{$error}</b></span><br>
                {/foreach}
            {/if} 
        </div>
        <br>
    {/if} 
{/foreach}
 </div>
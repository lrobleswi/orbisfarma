{*
 *  2019-2020 FARMALISTO
 *
 *  @author    Jhonny Romero <jromeroarenales@gmail.com>
 *  @copyright 2017-2020 FARMALISTO
 *  @license   https://www.farmalisto.com.mx/ - prestashop module orbisfarma
*}

<div class="collapse" id="mazsalud-card" style="padding: 1.6rem;
     background: #f1f1f1;
     background-color: rgb(241, 241, 241);
     background-position-x: 0%;
     background-position-y: 0%;
     background-repeat: repeat;
     background-attachment: scroll;
     background-image: none;
     background-size: auto;
     background-origin: padding-box;
     background-clip: border-box;
     vertical-align: middle;">
    <form id="add-card" data-link-action="add-card" method="post">
        <input type="hidden" name="token" value="{$static_token}">
        <input type="hidden" name="url_add_card" value="{$url_add_card}">
        <input type="hidden" name="request_uri" value="{$request_uri}">
        <input type="hidden" name="redirect" value="0">
        <input type="hidden" name="plan" value="{$plan}">
        <input type="hidden" name="id_service" value="{$id_service}">
        <input class="promo-input" type="text" name="card_number" placeholder="{l s='ENTER YOUR MAZSALUD CARD' mod='orbisfarma'}" style="color: #232323;
               border: 1px solid #7a7a7a;
               height: 2.5rem;
               text-indent: .625rem;
               width: 60%;
               vertical-align: middle;">
        <button type="submit" class="btn btn-primary add-card"><span>{l s='Add' d='Shop.Theme.Actions'}</span></button>
    </form>

    {if isset($errors)}
    <div class="alert alert-danger js-error" role="alert">
        <i class="material-icons">&#xE001;</i><span class="ml-1 js-error-text"></span>
    </div>
    {/if}
    <p style="padding-top: 5px;color:red;font-size: 11px;">Para agregar tu tarjeta, debes iniciar sesión en Farmalisto.</p>
</div>
<br>
<br>
{**
 *  2019-2020 FARMALISTO
 *
 *  @author    Jhonny Romero <jromeroarenales@gmail.com>
 *  @copyright 2017-2020 FARMALISTO
 *  @license   https://www.farmalisto.com.mx/ - prestashop module orbisfarma
 *}
{foreach $plans as $plan}
<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="order-slips-link" href="{$url_{$plan.name}}">
    <span class="link-item">
        <img width="180" height="47" src="/modules/orbisfarma/views/img/front/{$plan.name}.svg" alt="{$plan.description}" style="margin-bottom: 15px;filter: gray; -webkit-filter: grayscale(1); filter: grayscale(1);"/>
        <div>{l s='My card' mod='orbisfarma'}</div>
    </span>
</a>
{/foreach}
{**
 *  2019-2020 FARMALISTO
 *
 *  @author    Jhonny Romero <jromeroarenales@gmail.com>
 *  @copyright 2017-2018 FARMALISTO
 *  @license   https://www.farmalisto.com.mx/ - prestashop module orbisfarma
 *}
{extends file='page.tpl'}

{block name='notifications'}{/block}

{block name='page_title'}
  {if $editing}
    {l s='Update my card' mod='orbisfarma'}
  {else}
    {l s='New card' mod='orbisfarma'}
  {/if}
{/block}

{block name='page_content_top'}
      {block name='customer_notifications'}
        {include file='_partials/notifications.tpl'}
      {/block}
    {/block}

{block name='page_content'}
  <div class="card-orbisfarma-form">
    {render file="module:orbisfarma/views/templates/front/_partials/orbisfarma-form.tpl" ui=$card_form }
  </div>
{/block}

{block name='page_footer'}
  {block name='my_account_links'}
    {include file='customer/_partials/my-account-links.tpl'}
  {/block}
{/block}
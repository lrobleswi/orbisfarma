{**
 *  2019-2020 FARMALISTO
 *
 *  @author    Jhonny Romero <jromeroarenales@gmail.com>
 *  @copyright 2017-2018 FARMALISTO
 *  @license   https://www.farmalisto.com.mx/ - prestashop module orbisfarma
 *}
 
{block name="orbisfarma_form"}
  <div class="js-card-orbisfarma-form">
    {block name="orbisfarma_form_url"}
    <form
        id="orbisfarma-card-form"
      method="POST"
      action="{$url}"
      data-id-customer="{$id_customer}"
      data-refresh-url="{url entity='orbisfarma' params=['ajax' => 1, 'action' => 'orbisfarmaForm']}"
    >
    {/block}

      {block name="orbisfarma_form_fields"}
        <section class="form-fields">
          {block name='form_fields'}
            {foreach from=$formFields item="field"}
              {block name='form_field'}
                {form_field field=$field}
              {/block}
            {/foreach}
          {/block}
        </section>
      {/block}

      {block name="orbisfarma_form_footer"}
      <footer class="form-footer clearfix">
        <input type="hidden" name="submitCard" value="1">
        {block name='form_buttons'}
          <button id="orbisfarma-cart-save" class="btn btn-primary float-xs-right" type="submit" class="form-control-submit">
            {l s='Save' d='Shop.Theme.Actions'}
          </button>
        {/block}
      </footer>
      {/block}

    </form>
  </div>
{/block}

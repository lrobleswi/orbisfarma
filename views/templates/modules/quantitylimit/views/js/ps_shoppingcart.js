/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from BSofts.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the BSofts is strictly forbidden.
 *
 * @author    BSofts Inc.
 * @copyright Copyright 2018 © BSofts Inc.
 * @license   Single domain commerical license
 * @package   quantitylimit
 */

$(document).ready(function () {
    prestashop.blockcart = prestashop.blockcart || {};

    var showModal = prestashop.blockcart.showModal || function (modal) {
        var $body = $('body');
        $body.append(modal);
        $body.one('click', '#blockcart-modal', function (event) {
            if (event.target.id === 'blockcart-modal') {
                $(event.target).remove();
            }
        });
    };

    $(document).ready(function () {
        prestashop.on(
                'updateCart',
                function (event) {
                    var refreshURL = $('.blockcart').data('refresh-url');
                    var requestData = {};

                    if (event && event.reason) {
                        requestData = {
                            id_product_attribute: event.reason.idProductAttribute,
                            id_product: event.reason.idProduct,
                            action: event.reason.linkAction
                        };
                    }

                    if (typeof event.reason.idProduct !== 'undefined') {
                        $.post(refreshURL, requestData).then(function (resp) {
                            $('.blockcart').replaceWith($(resp.preview).find('.blockcart'));
                            if (resp.modal) {
                                showModal(resp.modal);
                            }
                        }).fail(function (resp) {
                            prestashop.emit('handleError', {eventType: 'updateShoppingCart', resp: resp});
                        });
                    }
                    var URLfooter = $('#footer-orbisfarma').data('footerOrbisfarma');
                    requestDataFooter = {
                        action: 'get-footer'
                    };
                    setTimeout(function () {
                        $.post(URLfooter, requestDataFooter).then(function (respfooter) {
                            $("#footer-orbisfarma").replaceWith(respfooter.footer);
                        }).fail(function (respfooter) {
                            prestashop.emit('handleError', {eventType: 'updateShoppingCart', resp: respfooter});
                        });
                    }, 1000);
                }
        );
    });
});

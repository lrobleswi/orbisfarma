    <li>
    <a href="javascript:void(0)" title="{l s='My Account'}" rel="nofollow" data-toggle="modal" data-target="#moda_quick_login">
      {l s='My Account'}</a></li>
    <li>
       <a href="{$link->getPageLink('cart',null,Context::getContext()->language->id,['action' => 'show'],false,null,true)}" title="{l s='My Cart'}" rel="nofollow">
        {l s='My Cart'}
        </a>
    </li>
    {hook h='linkwishlist'}
    <li>
       <a href="{$urls.pages.contact}" title="{l s='Contact us'}" rel="nofollow">
        {l s='Contact us'}
        </a>
    </li>
    {if $logged}
      <li>
      <a 
        href="javascript:void(0)"
        title="{l s='View my customer account'}"
        rel="nofollow"
        data-toggle="modal" data-target="#moda_quick_login"
      >
        {$customerName}
      </a>
      </li>
        {foreach $plans as $plan}
            {if isset($plan.active) && $plan.active}
                <li class="smooth02"><a href="{$link->getModuleLink('orbisfarma', 'account-cards-orbisfarma', ['id' => $plan.id_orbisfarma_service])}" title="Administrar mi tarjeta de beneficios" rel="nofollow">{l s='Tarjeta '|cat:$plan.description mod='orbisfarma'}</a></li>
            {/if}
        {/foreach}
      <li>
      <a 
        href="{$logout_url}"
        rel="nofollow"
      >
        {l s='Sign out'}
      </a>
      </li>
    {else}
    <li>
      <a 
        href="javascript:void(0)"
        title="{l s='Log in to your customer account'}"
        rel="nofollow"
        data-toggle="modal" data-target="#moda_quick_login"
      >
        {l s='Sign in'}
      </a>
      </li>
    {/if}
    
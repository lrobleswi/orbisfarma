<?php

/**
 *  2020-2021 FARMALISTO
 *
 *  @author    Jhonny Romero <jromeroarenales@gmail.com>
 *  @copyright 2017-2018 FARMALISTO
 *  @license   https://www.farmalisto.com.mx/ - prestashop module orbisfarma
 */
use Symfony\Component\Translation\TranslatorInterface;

require_once _PS_MODULE_DIR_ . 'orbisfarma/classes/OrbisfarmaCustomerCard.php';

class OrbisfarmaFormatter implements FormFormatterInterface {

    private $translator;
    private $language;
    private $card_is_required = false;
    public $module;

    public function __construct(
            TranslatorInterface $translator, Language $language, orbisfarma $module
    ) {
        $this->translator = $translator;
        $this->language = $language;
        $this->module = $module;
    }

    public function setCardRequired($card_is_required) {

        $this->card_is_required = $card_is_required;

        return $this;
    }

    public function getFormat() {

        $format = [];
        $format['id_customer'] = (new FormField())
                ->setName('id_customer')
                ->setType('hidden');
        $format['id_orbisfarma_service'] = (new FormField())
                ->setName('id_orbisfarma_service')
                ->setType('hidden');
        $format['number'] = (new FormField())
                ->setName('number')
                ->setType('text')
                ->setLabel("Numero de Tarjeta")
                ->setRequired($this->card_is_required);

        return $this->addConstraints($format);
    }

    private function addConstraints(array $format) {

        $constraints = OrbisfarmaCustomerCard::$definition['fields'];

        foreach ($format as $field) {
            if (!empty($constraints[$field->getName()]['validate'])) {
                $field->addConstraint(
                        $constraints[$field->getName()]['validate']
                );
            }
        }
        return $format;
    }

}

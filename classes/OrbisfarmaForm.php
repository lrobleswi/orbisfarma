<?php

/**
 *  2019-2020 FARMALISTO
 *
 *  @author    Jhonny Romero <jromeroarenales@gmail.com>
 *  @copyright 2017-2020 FARMALISTO
 *  @license   https://www.farmalisto.com.mx/ - prestashop module orbisfarma
 */
use Symfony\Component\Translation\TranslatorInterface;

require_once _PS_MODULE_DIR_ . 'orbisfarma/classes/OrbisfarmaFormatter.php';
require_once _PS_MODULE_DIR_ . 'orbisfarma/orbisfarma.php';

/**
 * StarterTheme TODO: B2B fields, Genders, CSRF.
 */
class OrbisfarmaForm extends AbstractForm {

    protected $template = 'module:orbisfarma/views/templates/front/_partials/orbisfarma-form.tpl';
    private $context;
    private $number;
    private $customer;
    private $id_orbisfarma_service;

    public function __construct(
            Smarty $smarty, Context $context, TranslatorInterface $translator, OrbisfarmaFormatter $formatter, $id_orbisfarma_service
    ) {
        parent::__construct(
                $smarty, $translator, $formatter
        );
        $this->context = $context;
        $this->id_orbisfarma_service = $id_orbisfarma_service;
    }

    public function setCard($number) {
        $this->number = $number;
        return $this;
    }

    public function loadCardById($number) {

        if (!$this->context->customer->isLogged() && !$this->context->customer->isGuest()) {
            return Tools::redirect('/index.php?controller=authentication');
        }
        $params['number'] = $number;

        return $this->fillWith($params);
    }

    public function fillWith(array $params = []) {

        return parent::fillWith($params);
    }

    public function validate() {

        $is_valid = true;

        if (($card = $this->getField('number'))) {
            if ($card->isRequired()) {
                if (empty($card->getValue())) {
                    $card->addError($this->l('Please enter the number card of your plan'));
                    $is_valid = false;
                }
            }
        }
        $this->validateFieldsLengths();
        return $is_valid && parent::validate();
    }

    protected function validateFieldsLengths() {
        $this->validateFieldLength('number', 13, $this->getCardMaxLengthViolationMessage());
    }

    /**
     * @param $fieldName
     * @param $maximumLength
     * @param $violationMessage
     */
    protected function validateFieldLength($fieldName, $maximumLength, $violationMessage) {
        $numberField = $this->getField($fieldName);
        if (strlen($numberField->getValue()) > $maximumLength) {
            $numberField->addError($violationMessage);
        }
    }

    /**
     * @return mixed
     */
    protected function getCardMaxLengthViolationMessage() {
        return $this->translator->trans(
                        'The %1$s field is too long (%2$d chars max).', array('de numero de tarjeta', 13), 'Shop.Notifications.Error'
        );
    }

    public function submit() {

        if (!$this->validate()) {
            return false;
        }
        if (self::saveCart($this->getValue('number'), $this->getValue('id_orbisfarma_service'))) {
            return true;
        } else {
            return false;
        }
        $this->setCard($this->formFields['number']['value']);

        return true;
    }

    /**
     * Save Cart
     * 
     * @param type $cartnumber
     * @return boolean
     */
    public static function saveCart($cartnumber, $id_orbisfarma_service) {

        $context = Context::getContext();
        if (!empty($context->customer->id)) {
            if ($number = Orbisfarma::getRegisterCard($id_orbisfarma_service, $context->customer->id)) {

                return Db::getInstance()->update('orbisfarma_customer_card', array(
                            'number' => $cartnumber),
                                'id_customer = ' . $context->customer->id .
                                ' AND id_orbisfarma_service = ' . (int) $id_orbisfarma_service
                );
            } else {
                return Db::getInstance()->insert('orbisfarma_customer_card', array(
                            'id_customer' => $context->customer->id,
                            'id_orbisfarma_service' => (int) $id_orbisfarma_service,
                            'number' => $cartnumber)
                                , false, true, Db::REPLACE);
            }
        }
        return false;
    }

    public function getTemplateVariables() {

        $context = Context::getContext();

        if (!$this->formFields) {
            // This is usually done by fillWith but the form may be
            // rendered before fillWith is called.
            // I don't want to assign formFields in the constructor
            // because it accesses the DB and a constructor should not
            // have side effects.
            $this->formFields = $this->formatter->getFormat();
        }

        $this->setValue('id_customer', $context->customer->id);

        $formFields = array_map(
                function (FormField $item) {
            return $item->toArray();
        }, $this->formFields
        );
        if (empty($formFields['number']['value'])) {
            $formFields['number']['value'] = self::getCardNumber($this->id_orbisfarma_service);
        }
        if (empty($formFields['id_orbisfarma_service']['value'])) {
            $formFields['id_orbisfarma_service']['value'] = $this->id_orbisfarma_service;
        }

        return array(
            'action' => $this->action,
            'errors' => $this->getErrors(),
            'formFields' => $formFields,
        );
    }

    public static function getCardNumber($id_orbisfarma_service) {
        $context = Context::getContext();
        $sql = new DbQuery();
        $sql->select('number');
        $sql->from('orbisfarma_customer_card', 'c');
        $sql->where('c.id_customer =' . (int) $context->customer->id);
        $sql->where('c.id_orbisfarma_service =' . (int) $id_orbisfarma_service);
        return Db::getInstance()->getValue($sql);
    }

    public static function existCardNumber($card) {
        $sql = new DbQuery();
        $sql->select('number');
        $sql->from('orbisfarma_customer_card', 'c');
        $sql->where("c.number = '" . pSQL($card) . "'");
        return Db::getInstance()->getValue($sql);
    }

}
<?php

/**
 *  2019-2020 FARMALISTO
 *
 *  @author    Jhonny Romero <jromeroarenales@gmail.com>
 *  @copyright 2017-2020 FARMALISTO
 *  @license   https://www.farmalisto.com.mx/ - prestashop module orbisfarma
 */
class OrbisfarmaCustomerCard extends ObjectModel {

    /** @var int $id Customer ID */
    public $id_customer;

    /** @var int $type Type ID Service*/
    public $id_orbisfarma_service;

    /** @var int $number Number Card */
    public $number;

    /** @var string Object creation date */
    public $date_add;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'orbisfarma_customer_card',
        'primary' => 'number',
        'fields' => array(
            'id_customer' => array('type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId', 'copy_post' => false),
            'id_orbisfarma_service' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'number' => array('type' => self::TYPE_STRING, 'validate' => 'isNumericOnly', 'required' => true, 'size' => 13),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
        ),
    );

}

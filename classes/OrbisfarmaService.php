<?php

class OrbisfarmaService extends ObjectModel {

    /** @var int Orbisfarma service ID */
    public $id_orbisfarma_service;

    /** @var string name */
    public $name;

    /** @var string description */
    public $description;

    /** @var string key */
    public $key;

    /** @var string key sandbox */
    public $key_sandbox;

    /** @var string wsdl */
    public $wsdl;

    /** @var string wsdl sandbox */
    public $wsdl_sandbox;

    /** @var bool plan environment production */
    public $prod_active = false;

    /** @var bool plan statuts */
    public $active = false;

    /** @var string Object creation date */
    public $date_add;

    /** @var string Object last modification date */
    public $date_upd;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'orbisfarma_service',
        'primary' => 'id_orbisfarma_service',
        'multilang' => false,
        'multilang_shop' => true,
        'fields' => array(
            /* Classic fields */
            'id_orbisfarma_service' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'name' => array('type' => self::TYPE_STRING, 'size' => 255),
            'description' => array('type' => self::TYPE_STRING, 'size' => 255),
            'key' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 255),
            'key_sandbox' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 255),
            'wsdl' => array('type' => self::TYPE_STRING, 'validate' => 'isUrl', 'size' => 255),
            'wsdl_sandbox' => array('type' => self::TYPE_STRING, 'validate' => 'isUrl', 'size' => 255),
            'prod_active' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'active' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
            'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
        ),
    );

}

<?php

/**
 *  2019-2020 FARMALISTO
 *
 *  @author    Jhonny Romero <jromeroarenales@gmail.com>
 *  @copyright 2017-2020 FARMALISTO
 *  @license   https://www.farmalisto.com.mx/ - prestashop module orbisfarma
 */
if (!defined('_PS_VERSION_')) {
    exit;
}

class Orbisfarma extends Module {

    const ORBISFARMA_PATH_LOG = _PS_ROOT_DIR_ . "/../logs/modules/orbisfarma/log";

    public function __construct() {

        $this->name = 'orbisfarma';
//        $this->tab = 'pricing_promotion';
        $this->version = '3.0.0';
        $this->author = 'Farmalisto';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Origis Webservice Discounts');
        $this->description = $this->l('Integration with Origis Discounts Webservices');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall this module?');
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
    }

    /**
     * Install module 
     *
     * @return boolean
     */
    public function install() {

        if (Configuration::get('PS_COUNTRY_DEFAULT') == 145) {

            if (extension_loaded('curl') == false) {
                $this->_errors[] = $this->l('You have to enable the cURL extension on your server to install this module');
                return false;
            }
            // Install SQL
            if (!$this->installSQL()) {
                return false;
            }
            // Install default
            if (!parent::install()) {
                return false;
            }
            // Registration hook
            if (!$this->registrationHook()) {
                return false;
            }
            // Variable of configuration
            if (!$this->createConfiguration()) {
                return false;
            }
            // Add Menu
            if (!$this->addMenu()) {
                return false;
            }
            // Add Submen��
            if (!$this->addSubMenu()) {
                return false;
            }
            // Add custom templates
            if (!$this->addCustomTemplates()) {
                return false;
            }
            return true;
        } else {
            $this->_errors[] = $this->l('This module is only to be installed in Mexico');
            return false;
        }
    }

    /**
     * Unistall module 
     *
     * @return boolean
     */
    public function uninstall() {

        // Uninstall Default
        if (!parent::uninstall()) {
            return false;
        }
        //Delete variables in Configuration
        if (!$this->deleteConfiguration()) {
            return false;
        }
        //Uninstall DataBase
        if (!$this->uninstallSQL()) {
            return false;
        }
        return true;
    }

    /**
     * Register hooks when the module is installed
     *
     * @return boolean
     */
    private function registrationHook() {

        $res = true;
        $res &= $this->registerHook('header');
        $res &= $this->registerHook('actionValidateOrder');
        $res &= $this->registerHook('actionOrderStatusPostUpdate');
        $res &= $this->registerHook('actionCartSave');
        $res &= $this->registerHook('displayShoppingCartFooter');
        $res &= $this->registerHook('displayCustomerAccount');
        $res &= $this->registerHook('actionUpdatediscountsOrbisfarma');

        return $res;
    }

    /**
     * Create tables when the module in install
     *
     * @return boolean
     */
    private function installSQL() {
        $res = true;
        include_once (dirname(__FILE__) . '/sql/install.php');
        return $res;
    }

    /**
     * Delete tables when the module is uninstall
     *
     * @return boolean
     */
    private function uninstallSQL() {

        $res = true;
        include_once (dirname(__FILE__) . '/sql/uninstall.php');
        return $res;
    }

    /**
     * Create parameters of Configuration
     * 
     * @return boolean
     */
    private function createConfiguration() {
        self::logtxt(" NOVO ENTRO createConfiguration()");

        $response = true;
        foreach (self::getAllsPlans() as $plan) {

            $plan_name = strtoupper($plan['name']);
            if (
                    !Configuration::updateValue('ORBISFARMA_WSDL_' . $plan_name, $plan['wsdl']) ||
                    !Configuration::updateValue('ORBISFARMA_WSDL_SANDBOX_' . $plan_name, $plan['wsdl_sandbox']) ||
                    !Configuration::updateValue('ORBISFARMA_ENABLED_' . $plan_name, $plan['active']) ||
                    !Configuration::updateValue('ORBISFARMA_PRODUCTION_ENVIRONMENT_' . $plan_name, 0) ||
                    !Configuration::updateValue('ORBISFARMA_NAME_' . $plan_name, $plan['name']) ||
                    !Configuration::updateValue('ORBISFARMA_KEY_' . $plan_name, $plan['key']) ||
                    !Configuration::updateValue('ORBISFARMA_KEY_SANDBOX_' . $plan_name, $plan['key_sandbox']) ||
                    !Configuration::updateValue('ORBISFARMA_STOREID_' . $plan_name, 1) ||
                    !Configuration::updateValue('ORBISFARMA_POSID_' . $plan_name, 1) ||
                    !Configuration::updateValue('ORBISFARMA_EMPLOYEEID_' . $plan_name, 1) ||
                    !Configuration::updateValue('ORBISFARMA_MSM_DISC_' . $plan_name, 'Descuento aplicado en plan ' . $plan['name']) ||
                    !Configuration::updateValue('ORBISFARMA_MSM_GIFT_' . $plan_name, 'Regalo aplicado en plan ' . $plan['name'])
            ) {
                $response = false;
                break;
            }
        }
        return $response;
    }

    /**
     * Delete configuration's variables when the module is un-install
     *
     * @return boolean
     */
    private function deleteConfiguration() {

        $response = true;
        foreach (self::getAllsPlans() as $plan) {

            $plan_name = strtoupper($plan['name']);
            if (
                    !Configuration::deleteByName('ORBISFARMA_WSDL_' . $plan_name) ||
                    !Configuration::deleteByName('ORBISFARMA_WSDL_SANDBOX_' . $plan_name) ||
                    !Configuration::deleteByName('ORBISFARMA_ENABLED_' . $plan_name) ||
                    !Configuration::deleteByName('ORBISFARMA_PRODUCTION_ENVIRONMENT_' . $plan_name) ||
                    !Configuration::deleteByName('ORBISFARMA_NAME_' . $plan_name) ||
                    !Configuration::deleteByName('ORBISFARMA_KEY_' . $plan_name) ||
                    !Configuration::deleteByName('ORBISFARMA_KEY_SANDBOX_' . $plan_name) ||
                    !Configuration::deleteByName('ORBISFARMA_STOREID_' . $plan_name) ||
                    !Configuration::deleteByName('ORBISFARMA_POSID_' . $plan_name) ||
                    !Configuration::deleteByName('ORBISFARMA_EMPLOYEEID_' . $plan_name) ||
                    !Configuration::deleteByName('ORBISFARMA_MSM_DISC_' . $plan_name) ||
                    !Configuration::deleteByName('ORBISFARMA_MSM_GIFT_' . $plan_name)
            ) {
                $response = false;
                break;
            }
        }
        return $response;
    }

    private function addCustomTemplates() {

        $this->custom_templates = array(
            'modules/ps_customersignin/ps_customersignin.tpl',
        );
        $success = true;
        foreach ($this->custom_templates as $ct) {
            $destination = _PS_THEME_DIR_ . $ct;
            self::logtxt(" DESTINO A COPIAR: " . $destination);
            self::logtxt(" DESTINO EXISTE EN CACHE?: " . (int) Tools::file_exists_cache($destination));
            $custom_template = _PS_MODULE_DIR_ . $this->name . "/views/templates/" . $ct;
            self::logtxt(" ORIGEN A COPIAR: " . $custom_template);
            self::logtxt(" ORIGEN A COPIAR EXISTE EN CACHE?: " . (int) Tools::file_exists_cache($custom_template));

            // @TODO: this should be improved by adding ability to report errors
            if (Tools::file_exists_cache($custom_template)) {
                $template_directory = dirname($destination);
                self::logtxt(" DIRECTORIO DESTINO: " . $template_directory);
                if (!Tools::file_exists_no_cache($template_directory)) {

                    self::logtxt(" ENTRO A CREAR EL DIRECTORRIO : " . $template_directory);
                    $success = $success && @mkdir($template_directory, 0775, true);
                    self::logtxt(" CREO EL DIRECTORRIO : " . (int) $success);
                }
                $success = $success && @copy($custom_template, $destination);
                self::logtxt(" COPIO EL DIRECTORRIO : " . (int) $success);
                self::logtxt(" EXISTE EN CACHE? ORIGEN: " . _PS_ROOT_DIR_ . '/config/index.php');
                self::logtxt(" EXISTE EN CACHE2? DESTINO: " . $template_directory . '/index.php');
                do {
                    if (Tools::file_exists_cache(_PS_ROOT_DIR_ . '/config/index.php') && !Tools::file_exists_cache($template_directory . '/index.php')) {
                        $success = $success && @copy(_PS_ROOT_DIR_ . '/config/index.php', $template_directory . '/index.php');
                    }
                    $template_directory = dirname($template_directory);
                    self::logtxt(" LONGITUD ROOT DIR: " . strlen(_PS_ROOT_DIR_));
                    self::logtxt(" LONGITUD TEMPLATE DIR: " . strlen($template_directory));
                } while (strlen(_PS_ROOT_DIR_) < strlen($template_directory));
            }
        }
        return $success;
    }

    /**
     * Install Menu
     * @return boolean
     */
    public function addMenu() {
        $tab = new Tab();

        foreach (Language::getLanguages(true) as $lang) {
            switch ($lang['iso_code']) {
                case 'cb':
                    $tab->name[$lang['id_lang']] = 'Orbisfarma';
                    break;
                case 'pe':
                    $tab->name[$lang['id_lang']] = 'Orbisfarma';
                    break;
                case 'mx':
                    $tab->name[$lang['id_lang']] = 'Orbisfarma';
                    break;
                default:
                    $tab->name[$lang['id_lang']] = 'Orbisfarma';
            }
        }

        $tab->class_name = 'AdminOrbisfarma';
        $tab->id_parent = Tab::getIdFromClassName('IMPROVE');
        $tab->module = $this->name;

        if ($tab->add() && Db::getInstance()->execute(' UPDATE `' . _DB_PREFIX_ . 'tab` SET `icon` = "card_membership" WHERE `id_tab` = "' . (int) $tab->id . '"')) {
            return true;
        } else {
            return true;
        }
    }

    /**
     * Install Submenu
     * @return boolean
     */
    public function addSubMenu() {
        $arraySubMenu = array(
            array(
                'name' => 'Plans',
                'name_cb' => 'Planes',
                'name_pe' => 'Planes',
                'name_mx' => 'Planes',
                'class_name' => 'AdminPlans',
                'active' => 1
            )
        );

        foreach ($arraySubMenu as $subMenu) {
            $tab = new Tab();
            foreach (Language::getLanguages(true) as $lang) {
                $tab->name[$lang['id_lang']] = (isset($subMenu['name_' . $lang['iso_code']]) ? $subMenu['name_' . $lang['iso_code']] : $subMenu['name']);
            }
            $tab->class_name = $subMenu['class_name'];
            $tab->id_parent = (int) empty($subMenu['id_parent']) ? Tab::getIdFromClassName('AdminOrbisfarma') : $subMenu['id_parent'];
            $tab->module = $this->name;
            $tab->active = (isset($subMenu['active']) ? $subMenu['active'] : 0);
            $tab->add();
        }

        return true;
    }

    /**
     * Load the configuration form
     */
    public function getContent() {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool) Tools::isSubmit('submitOrbisfarmaModule')) == true) {
            $this->postProcess();
        }
        return $this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm() {

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->title = 'Orbisfarma';
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitOrbisfarmaModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
                . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->toolbar_btn['save'] = array(
            'href' => $helper->currentIndex,
            'desc' => $this->trans('Save', array(), 'Admin.Global')
        );

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        $forms = array();
        foreach (self::getAllsPlans() as $key => $plan) {
            $forms[$key] = $this->getConfigForm($plan);
        }

        return $helper->generateForm($forms);
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm($plan) {

        $arrayConfig = array();

        $plan_name = strtoupper($plan['name']);
        $name_display = ucfirst($plan['description']);
        $arrayConfig = array(
            array(
                'type' => 'switch',
                'label' => $this->trans('Enabled', [], 'Admin.Global') . ' ' . $name_display,
                'name' => 'ORBISFARMA_ENABLED_' . $plan_name,
                'required' => false,
                'class' => 't',
                'is_bool' => true,
                'values' => array(
                    array(
                        'id' => 'prod_on_' . $plan_name,
                        'value' => 1,
                        'label' => $this->trans('Enabled', array(), 'Admin.Global')
                    ),
                    array(
                        'id' => 'prod_off_' . $plan_name,
                        'value' => 0,
                        'label' => $this->trans('Disabled', array(), 'Admin.Global')
                    ),
                ),
                'hint' => $this->l('Enable or disable the production environment ') . $name_display
            ),
            array(
                'type' => 'switch',
                'label' => $this->l('Producction environment ') . $name_display,
                'name' => 'ORBISFARMA_PRODUCTION_ENVIRONMENT_' . $plan_name,
                'required' => false,
                'class' => 't',
                'is_bool' => true,
                'values' => array(
                    array(
                        'id' => 'environment_on_' . $plan_name,
                        'value' => 1,
                        'label' => $this->trans('Enabled', array(), 'Admin.Global')
                    ),
                    array(
                        'id' => 'environment_off_' . $plan_name,
                        'value' => 0,
                        'label' => $this->trans('Disabled', array(), 'Admin.Global')
                    ),
                ),
                'hint' => $this->l('Enable or disable the production environment ') . $name_display
            ),
            array(
                'type' => 'text',
                'prefix' => '<i class="icon icon-link"></i>',
                'name' => 'ORBISFARMA_WSDL_' . $plan_name,
                'label' => 'WSDL ' . $name_display,
                'required' => true,
                'col' => 4,
                'hint' => $this->l('Enter the Web service url'),
            ),
            array(
                'type' => 'text',
                'prefix' => '<i class="icon icon-link"></i>',
                'name' => 'ORBISFARMA_WSDL_SANDBOX_' . $plan_name,
                'label' => 'WSDL Sandbox ' . $name_display,
                'required' => true,
                'col' => 4,
                'hint' => $this->l('Enter the Web service url Sandbox'),
            ),
            array(
                'type' => 'text',
                'prefix' => '<i class="icon icon-key"></i>',
                'name' => 'ORBISFARMA_KEY_' . $plan_name,
                'label' => 'KEY ' . $name_display,
                'required' => true,
                'col' => 4,
                'hint' => $this->l('Enter the key ') . $name_display,
            ),
            array(
                'type' => 'text',
                'prefix' => '<i class="icon icon-key"></i>',
                'name' => 'ORBISFARMA_KEY_SANDBOX_' . $plan_name,
                'label' => 'KEY Sandbox ' . $name_display,
                'required' => true,
                'col' => 4,
                'hint' => $this->l('Enter the key Sandbox ') . $name_display,
            ),
            array(
                'col' => 4,
                'type' => 'text',
                'name' => 'ORBISFARMA_STOREID_' . $plan_name,
                'label' => 'Store ID ' . $name_display,
                'hint' => $this->l('Identifier of the branch, store or point of sale where the card is found'),
            ),
            array(
                'col' => 4,
                'type' => 'text',
                'name' => 'ORBISFARMA_POSID_' . $plan_name,
                'label' => 'POS ID ' . $name_display,
                'hint' => $this->l('Unique identifier of the cashier or POS in the branch'),
            ),
            array(
                'col' => 4,
                'type' => 'text',
                'name' => 'ORBISFARMA_EMPLOYEEID_' . $plan_name,
                'label' => $this->trans('Employee', array(), 'Admin.Global') . ' ID ' . $name_display,
                'hint' => $this->l('Unique identifier of the employee, clerk or cashier in the branch'),
            ),
            array(
                'col' => 4,
                'type' => 'text',
                'name' => 'ORBISFARMA_MSM_DISC_' . $plan_name,
                'label' => $this->l('Message of Discount ') . $name_display,
                'hint' => $this->l('Message to show to client in case of Discount'),
            ),
            array(
                'col' => 4,
                'type' => 'text',
                'name' => 'ORBISFARMA_MSM_GIFT_' . $plan_name,
                'label' => $this->l('Message of Gift ') . $name_display,
                'hint' => $this->l('Message to show to client in case of Gift'),
            )
        );


        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('credentials settings ') . $name_display,
                    'icon' => 'icon-cogs'
                ),
                'input' => $arrayConfig,
                'submit' => array(
                    'title' => $this->l('Save')
                )
            )
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues() {

        $arrayConfig = array();
        foreach (self::getAllsPlans() as $plan) {

            $plan_name = strtoupper($plan['name']);
            $arrayConfig = array_merge($arrayConfig, array(
                'ORBISFARMA_WSDL_' . $plan_name => Configuration::get('ORBISFARMA_WSDL_' . $plan_name),
                'ORBISFARMA_WSDL_SANDBOX_' . $plan_name => Configuration::get('ORBISFARMA_WSDL_SANDBOX_' . $plan_name),
                'ORBISFARMA_ENABLED_' . $plan_name => Configuration::get('ORBISFARMA_ENABLED_' . $plan_name),
                'ORBISFARMA_PRODUCTION_ENVIRONMENT_' . $plan_name => (int) Configuration::get('ORBISFARMA_PRODUCTION_ENVIRONMENT_' . $plan_name),
                'ORBISFARMA_KEY_' . $plan_name => Configuration::get('ORBISFARMA_KEY_' . $plan_name),
                'ORBISFARMA_KEY_SANDBOX_' . $plan_name => Configuration::get('ORBISFARMA_KEY_SANDBOX_' . $plan_name),
                'ORBISFARMA_STOREID_' . $plan_name => Configuration::get('ORBISFARMA_STOREID_' . $plan_name),
                'ORBISFARMA_POSID_' . $plan_name => Configuration::get('ORBISFARMA_POSID_' . $plan_name),
                'ORBISFARMA_EMPLOYEEID_' . $plan_name => Configuration::get('ORBISFARMA_EMPLOYEEID_' . $plan_name),
                'ORBISFARMA_MSM_DISC_' . $plan_name => Configuration::get('ORBISFARMA_MSM_DISC_' . $plan_name),
                'ORBISFARMA_MSM_GIFT_' . $plan_name => Configuration::get('ORBISFARMA_MSM_GIFT_' . $plan_name)
            ));
        }
        return $arrayConfig;
    }

    /**
     * Save form data.
     */
    protected function postProcess() {

        foreach (self::getAllsPlans() as $plan) {
            $plan_name = strtoupper($plan['name']);

            if (!is_null(Tools::getValue('ORBISFARMA_WSDL_' . $plan_name)) && ((string) Tools::getValue('ORBISFARMA_WSDL_' . $plan_name) !== (string) Configuration::get('ORBISFARMA_WSDL_' . $plan_name) )) {
                Configuration::updateValue('ORBISFARMA_WSDL_' . $plan_name, Tools::getValue('ORBISFARMA_WSDL_' . $plan_name));
            }
            if (!is_null(Tools::getValue('ORBISFARMA_WSDL_SANDBOX_' . $plan_name)) && ((string) Tools::getValue('ORBISFARMA_WSDL_SANDBOX_' . $plan_name) !== (string) Configuration::get('ORBISFARMA_WSDL_SANDBOX_' . $plan_name) )) {
                Configuration::updateValue('ORBISFARMA_WSDL_SANDBOX_' . $plan_name, Tools::getValue('ORBISFARMA_WSDL_SANDBOX_' . $plan_name));
            }

            if (!is_null(Tools::getValue('ORBISFARMA_ENABLED_' . $plan_name)) && ((string) Tools::getValue('ORBISFARMA_ENABLED_' . $plan_name) !== (string) Configuration::get('ORBISFARMA_ENABLED_' . $plan_name) )) {
                Configuration::updateValue('ORBISFARMA_ENABLED_' . $plan_name, Tools::getValue('ORBISFARMA_ENABLED_' . $plan_name));
                Db::getInstance()->update(
                        'orbisfarma_service',
                        array(
                            'active' => (int) Tools::getValue('ORBISFARMA_ENABLED_' . $plan_name)
                        ),
                        '`id_orbisfarma_service` = ' . (int) $plan['id_orbisfarma_service'],
                        1
                );
            }
            if (!is_null(Tools::getValue('ORBISFARMA_PRODUCTION_ENVIRONMENT_' . $plan_name)) && ((string) Tools::getValue('ORBISFARMA_PRODUCTION_ENVIRONMENT_' . $plan_name) !== (string) Configuration::get('ORBISFARMA_PRODUCTION_ENVIRONMENT_' . $plan_name) )) {
                Configuration::updateValue('ORBISFARMA_PRODUCTION_ENVIRONMENT_' . $plan_name, Tools::getValue('ORBISFARMA_PRODUCTION_ENVIRONMENT_' . $plan_name));
                Db::getInstance()->update(
                        'orbisfarma_service',
                        array(
                            'prod_active' => (int) Tools::getValue('ORBISFARMA_PRODUCTION_ENVIRONMENT_' . $plan_name)
                        ),
                        '`id_orbisfarma_service` = ' . (int) $plan['id_orbisfarma_service'],
                        1
                );
            }
            if (!is_null(Tools::getValue('ORBISFARMA_KEY_' . $plan_name)) && ((string) Tools::getValue('ORBISFARMA_KEY_' . $plan_name) !== (string) Configuration::get('ORBISFARMA_KEY_' . $plan_name) )) {
                Configuration::updateValue('ORBISFARMA_KEY_' . $plan_name, Tools::getValue('ORBISFARMA_KEY_' . $plan_name));
            }
            if (!is_null(Tools::getValue('ORBISFARMA_KEY_SANDBOX_' . $plan_name)) && ((string) Tools::getValue('ORBISFARMA_KEY_SANDBOX_' . $plan_name) !== (string) Configuration::get('ORBISFARMA_KEY_SANDBOX_' . $plan_name) )) {
                Configuration::updateValue('ORBISFARMA_KEY_SANDBOX_' . $plan_name, Tools::getValue('ORBISFARMA_KEY_SANDBOX_' . $plan_name));
            }
            if (!is_null(Tools::getValue('ORBISFARMA_STOREID_' . $plan_name)) && ((string) Tools::getValue('ORBISFARMA_STOREID_' . $plan_name) !== (string) Configuration::get('ORBISFARMA_STOREID_' . $plan_name) )) {
                Configuration::updateValue('ORBISFARMA_STOREID_' . $plan_name, Tools::getValue('ORBISFARMA_STOREID_' . $plan_name));
            }
            if (!is_null(Tools::getValue('ORBISFARMA_POSID_' . $plan_name)) && ((string) Tools::getValue('ORBISFARMA_POSID_' . $plan_name) !== (string) Configuration::get('ORBISFARMA_POSID_' . $plan_name) )) {
                Configuration::updateValue('ORBISFARMA_POSID_' . $plan_name, Tools::getValue('ORBISFARMA_POSID_' . $plan_name));
            }
            if (!is_null(Tools::getValue('ORBISFARMA_EMPLOYEEID_' . $plan_name)) && ((string) Tools::getValue('ORBISFARMA_EMPLOYEEID_' . $plan_name) !== (string) Configuration::get('ORBISFARMA_EMPLOYEEID_' . $plan_name) )) {
                Configuration::updateValue('ORBISFARMA_EMPLOYEEID_' . $plan_name, Tools::getValue('ORBISFARMA_EMPLOYEEID_' . $plan_name));
            }
            if (!is_null(Tools::getValue('ORBISFARMA_MSM_DISC_' . $plan_name)) && ((string) Tools::getValue('ORBISFARMA_MSM_DISC_' . $plan_name) !== (string) Configuration::get('ORBISFARMA_MSM_DISC_' . $plan_name) )) {
                Configuration::updateValue('ORBISFARMA_MSM_DISC_' . $plan_name, Tools::getValue('ORBISFARMA_MSM_DISC_' . $plan_name));
            }
            if (!is_null(Tools::getValue('ORBISFARMA_MSM_GIFT_' . $plan_name)) && ((string) Tools::getValue('ORBISFARMA_MSM_GIFT_' . $plan_name) !== (string) Configuration::get('ORBISFARMA_MSM_GIFT_' . $plan_name) )) {
                Configuration::updateValue('ORBISFARMA_MSM_GIFT_' . $plan_name, Tools::getValue('ORBISFARMA_MSM_GIFT_' . $plan_name));
            }
        }

        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
     * Hook Action Validate Order
     * 
     * @param array() $params 
     * @return void The order is a sale NovoNordisk 
     */
    public function hookActionValidateOrder($params) {

        $id_customer = (int) $params['order']->id_customer;

        if (isset($id_customer) && $id_customer > 0) {
            foreach (self::getPlans() as $plan) {
                $id_orbisfarma_service = $plan['id_orbisfarma_service'];
                if (self::isCustomerPlan($id_orbisfarma_service, $id_customer)) {

                    $order = $params['order'];
                    $plan_name = $plan['name'];

                    $result = $this->setTransactionSale($order, $plan);

                    if ($card_number = self::getCustomerCard($id_orbisfarma_service, $order->id_customer) && $result) {

                        if (($result->errorid == 7 || $result->errorid == 8)) {
                            self::logtxt("hookActionValidateOrder Entro a renovar INIT, error: " . $result->errorid);

                            $responseInit = $this->setTransactionInit($card_number, $plan);
                            $cart = new Cart($order->id_cart);

                            if ($responseInit->errorid == 0) {
                                $responseQuote = $this->setTransactionQuote($cart, $plan);
                                if ($responseQuote->errorid == 0) {
                                    $result = $this->setTransactionSale($order, $plan);
                                }
                            }
                        }
                        if (Db::getInstance()->insert('orbisfarma_transaction_init', array(
                                    'id_orbisfarma_service' => $id_orbisfarma_service,
                                    'id_cart' => $order->id_cart,
                                    'id_order' => $order->id,
                                    'cardnumber' => $card_number,
                                    'transactionid' => self::getTransactionID($order->id_cart, $id_orbisfarma_service),
                                    'saleauthnumber' => $result->saleauthnumber,
                                    'errorid' => $result->errorid,
                                    'sended' => '1',
                                    'transactiondate' => $result->transactiondate,
                                    'date_add' => date('Y:m:d H:i:s')
                                        )
                                        , false, true, Db::REPLACE)) {
                            self::logtxt("INSERT orbisfarma_transaction_init OK");
                        } else
                            self::logtxt("INSERT orbisfarma_transaction_init ERROR");
                    }
                }
            }
        }
    }

    /**
     * Hook ActionOrder Status Post Update
     * 
     * @param array() $params
     * @return void Order is Canceled in NovoNordisk
     */
    public function hookActionOrderStatusPostUpdate($params) {

        self::logtxt("hookActionOrderStatusPostUpdate()");
        $new_os = $params['newOrderStatus'];

        if ($new_os->id == Configuration::get('PS_OS_CANCELED')) {
            $order = new Order($params['id_order']);
            $id_customer = $order->id_customer;

            if (isset($id_customer) && $id_customer > 0) {
                foreach (self::getPlans() as $plan) {
                    $id_orbisfarma_service = $plan['id_orbisfarma_service'];
                    if (self::isCustomerPlan($id_orbisfarma_service, $id_customer)) {

                        $order = $params['order'];
                        $plan_name = $plan['name'];

                        $order = new Order($params['id_order']);
                        if (self::getSaleauthnumber($order->id, $order->id_cart)) {
                            $reponseSaleReverse = $this->setSaleReverse($order, $plan);
                        } else {
                            self::logtxt("Esta orden no se puede cancelar ya que no tiene un Saleauthnumber.");
                        }
                    }
                }
            }
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader() {
        $this->context->cookie->{'add_card_mazsalud'} = false;

        if (Tools::isSubmit('controller') && Tools::getValue('controller') == 'cart' && Tools::isSubmit('action') && Tools::getValue('action') == 'show') {
            $this->context->controller->addJS($this->_path . '/views/js/orbisfarma.js');
            $this->context->cookie->{'return_url'} = $this->context->link->getPageLink('cart', null, null, array('action' => 'show'));
            return;
        }
        if (Tools::isSubmit("id_product") && Tools::isSubmit('controller') && Tools::getValue('controller') == 'product') {
            if (isset($this->context->customer) && $this->context->customer->isLogged() && self::isCustomerPlan(3, $this->context->customer->id)) {
                return;
            }
            $id_product = (int) Tools::getValue("id_product");
            $categories = Product::getProductCategories($id_product);
            $informations = Category::getCategoryInformation($categories);
            foreach ($informations as $information) {
                if (preg_match("/^astrazeneca/", strtolower($information['name']))) {
                    $this->context->controller->addJS($this->_path . '/views/js/orbisfarma.js');
                    $this->context->cookie->{'add_card_mazsalud'} = true;
                    $link = $this->context->link->getProductLink($id_product);
                    $this->context->cookie->{'return_url'} = $link;
                    $this->context->smarty->assign('url_add_card', $this->context->link->getModuleLink('orbisfarma', 'ajax'));
                    return;
                }
            }
        }
        if (Tools::isSubmit('controller') && Tools::getValue('controller') == 'category' && Tools::isSubmit('id_category')) {
            $id_category = (int) Tools::getValue('id_category');
            $this->context->cookie->{'return_url'} = $this->context->link->getCategoryLink($id_category);
        }
    }

    /**
     * Hook Display Customer Account
     * 
     * @return string Code HTLM to show
     */
    public function hookDisplayCustomerAccount() {

        $plans = self::getPlans();

        foreach ($plans as $plan) {
            $url = Context::getContext()->link->getModuleLink($this->name, 'account-cards-orbisfarma', array('id' => $plan['id_orbisfarma_service']));
            $this->context->smarty->assign('url_' . $plan['name'], $url);
        }
        $this->context->smarty->assign('plans', $plans);
        return $this->display(__FILE__, 'hookDisplayCustomerAccount.tpl');
    }

    /**
     * Hook Display Shopping Cart Footer
     * 
     * @param array() $params
     * @return string Code HTLM footer
     */
    public function hookDisplayShoppingCartFooter($params) {
        self::logtxt("hookDisplayShoppingCartFooter()");
        $this->context->smarty->assign('url_footer_orbisfarma', $this->context->link->getModuleLink('orbisfarma', 'ajax'));
        if (isset($this->context->customer) && $this->context->customer->isLogged() && Tools::getValue("controller") != 'order') {
            self::logtxt("Validating plans");
            
            $id_customer = empty($params['cart']->id_customer) ? $this->context->customer->id : $params['cart']->id_customer;
            $cart = empty($params['cart']) ? $this->context->cart : $params['cart'];
            $plans = self::getPlansCustomerEnabled($id_customer);
            foreach ($plans as $plan) {
                $_confirmations = array();
                $_errors = array();
                $keyDiscount = 0;
                $discounts = array();
                $gift_products = array();
                $plan_name = strtolower($plan['name']);

                self::logtxt("There is a quote");
                $products = Cart::getNbProducts((int) $cart->id);
                self::logtxt("CART getNbProducts 1: " . $products);
                if (isset($products) && $products > 0) {
                    if (isset($this->context->cookie->{'_confirmations_' . $plan_name})) {
                        $va = json_decode(unserialize($this->context->cookie->{'_confirmations_' . $plan_name}));
                        $_confirmations = array_unique(json_decode(unserialize($this->context->cookie->{'_confirmations_' . $plan_name})));
                    }
                    if (isset($this->context->cookie->{'_errors_' . $plan_name}))
                        $_errors = array_unique(json_decode(unserialize($this->context->cookie->{'_errors_' . $plan_name})));
                    if(!empty($_confirmations) || !empty($_errors)){

                        $this->context->smarty->assign('title', true);
                    }

                    $this->context->smarty->assign('form_confirmations_' . $plan_name, empty($_confirmations) ? null : $_confirmations);
                    $this->context->smarty->assign('form_errors_' . $plan_name, empty($_errors) ? null : $_errors);
                } else {
                    $this->context->smarty->assign('form_confirmations_' . $plan_name, null);
                    $this->context->smarty->assign('form_errors_' . $plan_name, null);
                }
                if (isset($context->cookie->{'_confirmations_' . $plan_name}))
                    unset($context->cookie->{'_confirmations_' . $plan_name});
                if (isset($context->cookie->{'_errors_' . $plan_name}))
                    unset($context->cookie->{'_errors_' . $plan_name});
            }
            $this->context->smarty->assign('plans', $plans);
            return $this->display(__FILE__, 'hookDisplayShoppingCartFooter.tpl');
        }
    }

    /**
     * Hook Display Customer Account
     * 
     * @return string Code HTLM to show
     */
    public function hookDisplayGuide($params) {
        if ($this->context->cookie->{'add_card_mazsalud'} == true) {
            $this->context->smarty->assign(['plan' => $this->name, 'id_service' => 3]);
            return $this->display(__FILE__, 'hookDisplayGuide.tpl');
        }
    }

    /**
     * Hook Display Customer Account
     * 
     * @return string Code HTLM to show
     */
    public function hookDisplayCardsOrbisfarma($params) {
        if (isset($this->context->customer) && $this->context->customer->isLogged()) {
            $id_customer = $this->context->customer->id;
            $attachment_plans = array();
            $plans_customer_orbisfarma = self::getPlansCustomerDisabled($id_customer);
            foreach ($plans_customer_orbisfarma as $key => $plan_customer_orbisfarma) {
                $plans_customer_orbisfarma[$key]['module'] = 'orbisfarma';
            }
            // var_dump( $plans_customer_orbisfarma);


            if ($lms = Module::getInstanceByName('lms') && Module::isEnabled('lms')) {
                $plans_customer_lms = lms::getPlansCustomerDisabled($id_customer);
                foreach ($plans_customer_lms as $key => $plan_customer_lms) {
                    $plans_customer_lms[$key]['module'] = 'lms';
                }
                $attachment_plans = array_merge($plans_customer_orbisfarma, $plans_customer_lms);
            } else {
                $attachment_plans = $plans_customer_orbisfarma;
            }

            if (!empty($attachment_plans)) {

                $this->context->smarty->assign('attachment_plans', $attachment_plans);
                $this->context->smarty->assign('url_add_card', $this->context->link->getModuleLink('orbisfarma', 'ajax'));
                return $this->display(__FILE__, 'hookDisplayCardsOrbisfarma.tpl');
            } else {
                return;
            }
        } else {
            $attachment_plans = array();
            $plans_orbisfarma = self::getPlans();
            foreach ($plans_orbisfarma as $key => $plan_orbisfarma) {
                $plans_orbisfarma[$key]['module'] = 'orbisfarma';
            }
            if ($lms = Module::getInstanceByName('lms') && Module::isEnabled('lms')) {
                $plans_lms = lms::getPlans();
                foreach ($plans_lms as $key => $plan_lms) {
                    $plans_lms[$key]['module'] = 'lms';
                }
                $attachment_plans = array_merge($plans_orbisfarma, $plans_lms);
            } else {
                $attachment_plans = $plans_orbisfarma;
            }
            if (!empty($attachment_plans)) {
                $this->context->smarty->assign('attachment_plans', $attachment_plans);
                $this->context->smarty->assign('url_add_card', $this->context->link->getModuleLink('orbisfarma', 'ajax'));
                return $this->display(__FILE__, 'hookDisplayCardsOrbisfarma.tpl');
            } else {
                return;
            }
        }
    }

    /**
     * Hook Display Customer Account
     * 
     * @return string Code HTLM to show
     */
    public function getMessage($params) {
        $this->context->smarty->assign(array('message' => $params['message']));
        return $this->display(__FILE__, 'getMessage.tpl');
    }

    /**
     * Hook Display Customer Account
     * 
     * @return string Code HTLM to show
     */
    public function hookActionFrontControllerSetMedia($params) {

        // self::logtxt("hookActionFrontControllerSetMedia() ");

        /* if (Tools::isSubmit("id_product") && Tools::isSubmit("controller") && Tools::getValue('controller') == 'product') {
          self::logtxt("hookActionFrontControllerSetMedia() ENTRO");
          $id_product = (int) Tools::getValue("id_product");
          $categories = Product::getProductCategories($id_product);
          $informations = Category::getCategoryInformation($categories);

          foreach ($informations as $information) {
          if (preg_match("/^astrazeneca/", strtolower($information['name']))) {

          $templateVars = array(
          'url_add_card' => $this->context->link->getModuleLink('orbisfarma', 'ajax'),
          'static_token' => Tools::getToken(false),
          'id_customer' => $this->context->customer->id,
          'is_customer_plan' => false
          );
          $this->context->smarty->assign($templateVars);
          }
          }
          }
          if (Tools::isSubmit('controller') && Tools::getValue('controller') == 'cart' && Tools::isSubmit('action') && Tools::getValue('action') == 'show') {
          $templateVars = array(
          'url_add_card' => $this->context->link->getModuleLink('orbisfarma', 'ajax'),
          'static_token' => Tools::getToken(false),
          'id_customer' => $this->context->customer->id,
          'is_customer_plan' => false,
          'plans' => self::getPlans()
          );
          $this->context->smarty->assign($templateVars);
          } */
    }

    /**
     * Hook Display Customer Account
     * 
     * @return string Code HTLM to show
     */
    public function hookActionUpdatediscountsOrbisfarma($params) {

        self::logtxt("hookActionUpdatediscountsOrbisfarma() ");

        if ((isset($this->context->customer) && $this->context->customer->isLogged()) && ( Tools::isSubmit("action") && (Tools::getValue("action") == "updateQty" || Tools::getValue("action") == "deleteProduct" || Tools::getValue("action") == "refresh"))) {

            self::logtxt("hookActionUpdatediscountsOrbisfarma() action = " . Tools::getValue("action"));
            $id_customer = empty($params['cart']->id_customer) ? $this->context->customer->id : $params['cart']->id_customer;
            $cart = empty($params['cart']) ? $this->context->cart : $params['cart'];
            if (Validate::isLoadedObject($cart) && $cart instanceof CartCore) {
                foreach (self::getPlans() as $plan) {
                    if ((int) $plan['active']) {
                        self::logtxt("hookActionUpdatediscountsOrbisfarma() quote_" . strtolower($plan['name']) . ": " . (int) $this->context->cookie->{'quote_' . strtolower($plan['name'])});
                        // 
                        if ((bool) self::isCustomerPlan($plan['id_orbisfarma_service'], $id_customer)) {
                            if (!(bool) $this->context->cookie->{'quote_' . strtolower($plan['name'])}) {
                                $this->context->cookie->{'quote_' . strtolower($plan['name'])} = true;
                                $products = Cart::getNbProducts((int) $cart->id);
                                self::logtxt("CART getNbProducts: " . $products);
                                if (isset($products) && $products > 0) {
                                    self::removeRules($this->context, $plan['name']);
                                    $this->setTransactionQuote($cart, $plan);
                                } else {
                                    self::logtxt("There are no products in the cart hookActionUpdatediscountsOrbisfarma() ");
                                }
                            }
                        } else {
                            self::removeRules($this->context, $plan['name']);
                            self::logtxt("No es usuario " . $plan['name'] . "hookActionUpdatediscountsOrbisfarma()");
                        }
                    }
                }
            }
        } else {
            self::logtxt("hookActionUpdatediscountsContacto NOT Logged, action = " . Tools::getValue("action"));
        }
        return true;
    }

    /**
     * 
     */
    public function hookActionCartSave($params) {

        self::logtxt("hookActionCartSave()");
        if ((isset($this->context->customer) && $this->context->customer->isLogged()) && ( Tools::isSubmit("action") && (Tools::getValue("action") == "updateQty" || Tools::getValue("action") == "deleteProduct" || Tools::getValue("action") == "refresh"))) {
            //TODO codigo de prueba 
            // if (true) {
            self::logtxt("hookActionCartSave() action = " . Tools::getValue("action"));
            $cart = isset($params['cart']) ? $params['cart'] : $this->context->cart;
            $customer = $this->context->customer;

            if (Validate::isLoadedObject($cart) && $cart instanceof CartCore && Validate::isLoadedObject($customer) && $customer instanceof CustomerCore) {
                foreach (self::getPlans() as $plan) {
                    if ((int) $plan['active']) {
                        if ((bool) self::isCustomerPlan($plan['id_orbisfarma_service'], $customer->id)) {
                            if (!(bool) $this->context->cookie->{'quote_' . strtolower($plan['name'])}) {

                                $this->context->cookie->{'quote_' . strtolower($plan['name'])} = true;
                                $products = Cart::getNbProducts((int) $cart->id);
                                if (isset($products) && $products > 0) {
                                    self::removeRules($this->context, $plan['name']);
                                    $this->setTransactionQuote($cart, $plan);
                                } else {
                                    $this->context->cookie->{'quote_' . strtolower($plan['name'])} = false;
                                    self::logtxt("There are no products in the cart hookActionCartSave() ");
                                }
                            } else {
                                self::logtxt("hookActionCartSave() There is already a quote for quote_" . strtolower($plan['name']));
                            }
                        } else {
                            self::removeRules($this->context, $plan['name']);
                            self::logtxt("No es usuario hookActionCartSave()");
                        }
                    }
                }
            }
        } else {
            self::logtxt("hookActionCartSave() NOT Logged, action = " . Tools::getValue("action"));
        }
    }

    /**
     * Set Transaction Init
     * 
     * @return array() Transation is Init response
     */
    public function setTransactionInit($card_number, $plan) {

        $id_orbisfarma_service = $plan['id_orbisfarma_service'];
        $plan_name = $plan['name'];
        self::logtxt("FUNCTION setTransactionInit() " . $plan_name . ", cardNumber: " . $card_number . ", id_orbisfarma_service: " . $id_orbisfarma_service);

        if (isset($card_number) && isset($id_orbisfarma_service)) {

            $config = self::getConfigurationOrbis($plan_name);

            $params = array(
                'cardnumber' => $card_number,
                'storeid' => $config['storeid'],
                'posid' => $config['posid'],
                'employeeid' => $config['employeeid'],
                'key' => self::getKeyWS($plan_name)
            );
            try {
                $server = self::getWSDL($plan_name);
                $client = new SoapClient($server);
                self::logtxt("SERVER: " . $server . ", id_orbisfarma_service: " . $id_orbisfarma_service);
                self::logtxt("Request setTransactionInit " . $plan_name . ": id_orbisfarma_service: " . $id_orbisfarma_service . ", id_cart: " . $this->context->cart->id . ": " . json_encode($params));
                $response = $client->setTransactionInit($params);
                $response2 = simplexml_load_string($response->setTransactionInitResult->any);
                $result = $response2->NewDataSet->FirstTable;
                self::logtxt("Response setTransactionInit " . $plan_name . ": id_orbisfarma_service: " . $id_orbisfarma_service . ", id_cart: " . $this->context->cart->id);
//                self::logtxt("Response setTransactionInit " . $plan_name . ": id_orbisfarma_service: " . $id_orbisfarma_service . ", id_cart: " . $this->context->cart->id . ": " . json_encode($result));
            } catch (SoapFault $fault) {
                self::logtxt("[SoapFault " . $fault->faultcode . "] setTransactionInit " . $fault->faultstring . ', el servicio no responde en este momento, por favor comuniquese a mesa de ayuda Orbisfarma ó inténtelo más tarde.');
                $_errors_{$plan_name}[] = '[SoapFault ' . $fault->faultcode . ' setTransactionInit] El servicio no responde en este momento, por favor comuniquese a mesa de ayuda Orbisfarma ó inténtelo más tarde.';
                $this->context->cookie->{'_errors_' . $plan_name} = serialize(json_encode($_errors_{$plan_name}));
                return;
            }


            Db::getInstance()->insert('orbisfarma_transaction_init', array(
                'id_orbisfarma_service' => $id_orbisfarma_service,
                'id_cart' => $this->context->cart->id,
                'id_order' => 0,
                'cardnumber' => $result->cardnumber,
                'transactionid' => $result->transactionid,
                'errorid' => $result->errorid,
                'transactiondate' => $result->transactiondate
                    )
                    , false, true, Db::REPLACE);

            if ($result->errorid != 0) {
                $_errors_{$plan_name}[] = htmlspecialchars($result->message);
                $this->context->cookie->{'_errors_' . $plan_name} = serialize(json_encode($_errors_{$plan_name}));
                return false;
            } else {
                return $result;
            }
        }
    }

    /**
     * Set Transaction Quote
     * 
     * @param Cart $cart 
     * @return void Transaction Quote
     */
    public function setTransactionQuote($cart, $plan) {

        self::logtxt("FUNCTION setTransactionQuote()");

        $id_orbisfarma_service = $plan['id_orbisfarma_service'];
        $plan_name = $plan['name'];

        if ($card = self::getCustomerCard($id_orbisfarma_service)) {

//            $cookie_quote = (bool) $this->context->cookie->{'quote_' . strtolower($plan_name)};
//            if (self::isActiveOrbisfarmaService($id_orbisfarma_service) && !$cookie_quote) {

            if (self::isActiveOrbisfarmaService($id_orbisfarma_service)) {

                $responseInit = $this->setTransactionInit($card, $plan);
                if (!empty($responseInit)) {
                    $transactionid = (string) $responseInit->transactionid;

                    if (isset($transactionid) && !empty($transactionid)) {

                        if (is_null($cart)) {
                            $cart = $this->context->cart;
                        }
                        $productsOrigisFormat = array();
                        $carditems = $this->getArrayItems((string) $responseInit->carditems);

                        if (isset($context->cookie->{'_confirmations_' . $plan_name}))
                            unset($context->cookie->{'_confirmations_' . $plan_name});
                        if (isset($context->cookie->{'_errors_' . $plan_name}))
                            unset($context->cookie->{'_errors_' . $plan_name});

                        $products = $cart->getProducts();
//                        $products = $cart->getProducts(false, false, null, false);

                        foreach ($products as $product) {
                            if (isset($product['reference']) && in_array($product['reference'], $carditems)) {
                                $productsOrigisFormat[] = implode(',', array($product['reference'], $product['quantity'], round($product['price'], 2), round($product['price'], 2)));
                            }
                        }
                        if ($this->context->cart->id) {
                            Db::getInstance()->delete('orbisfarma_transaction_quote', 'id_cart = ' . (int) $this->context->cart->id . ' AND id_orbisfarma_service = ' . (int) $id_orbisfarma_service);
                        }
                        if (!empty($productsOrigisFormat)) {

                            $transactionitems = implode('|', $productsOrigisFormat);
                            $config = self::getConfigurationOrbis($plan_name);

                            $params = array(
                                'cardnumber' => $card,
                                'storeid' => $config['storeid'],
                                'posid' => $config['posid'],
                                'employeeid' => $config['employeeid'],
                                'transactionid' => $transactionid,
                                'transactionitems' => $transactionitems,
                                'key' => self::getKeyWS($plan_name)
                            );

                            try {
                                $server = self::getWSDL($plan_name);
                                $client = new SoapClient($server);
                                self::logtxt("SERVER: " . $server . ", id_orbisfarma_service: " . $id_orbisfarma_service);
                                $response = $client->setTransactionQuote($params);
                                self::logtxt("Request setTransactionQuote " . $plan_name . ": id_cart: " . $this->context->cart->id . ": " . json_encode($params));
                                $response2 = simplexml_load_string($response->setTransactionQuoteResult->any);
                                $result = $response2->NewDataSet->FirstTable;
                                self::logtxt("Response setTransactionQuote " . $plan_name . ": id_cart: " . $this->context->cart->id . ": " . json_encode($result));
                            } catch (SoapFault $fault) {
                                self::logtxt("[SoapFault " . $fault->faultcode . "] setTransactionQuote " . $fault->faultstring . ', el servicio no responde en este momento, por favor comuniquese a mesa de ayuda Orbisfarma ó inténtelo más tarde.');
                                $_errors_{$plan_name}[] = '[SoapFault ' . $fault->faultcode . ' setTransactionQuote] El servicio no responde en este momento, por favor comuniquese a mesa de ayuda Orbisfarma ó inténtelo más tarde.';
                                $this->context->cookie->{'_errors_' . $plan_name} = serialize(json_encode($_errors_{$plan_name}));
                                return;
                            }

                            if ($result->errorid == 0) {

                                $this->context->cookie->{'quote_' . strtolower($plan_name)} = true;

                                $discounts = explode('|', $result->transactionitems);

                                $priceDiscount = array();
                                $priority = 1;

                                foreach ($discounts as $key => $discount) {

                                    if (!empty($discount)) {
                                        $discountArray = explode(',', $discount);
                                        $reference = $discountArray[0];
                                        $discountQuantity = $discountArray[1];
                                        $id_product = self::getIdByReference($reference);

                                        if (isset($id_product) && !empty($id_product)) {

                                            $discount = $discountArray[2];
                                            $giftQuantity = $discountArray[3];
                                            $points = $discountArray[4];
                                            $product_name = "No aplica";

                                            if ($discountQuantity > 0 && $discount > 0) {
                                                self::logtxt("ENTRO A AGREGAR DESCUENTO: " . ($key + 1) . "");
                                                self::addCartRuleDiscount($plan_name, $id_product, $discountQuantity, $discount, $priority, $result->cardnumber, $result->transactionid, $this->context);
                                                $priority++;
                                                foreach ($products as $key => $product_msg) {
                                                    if ($product_msg['reference'] == $reference) {
                                                        $product_name = $product_msg['name'];
                                                        break;
                                                    }
                                                }
                                                $_confirmations[] = htmlspecialchars($plan['description'] . ", aplica un descuento del  " . $discount . "% sobre " . $product_name . " como beneficio de afiliado.");
                                                $this->context->cookie->{'_confirmations_' . $plan_name} = serialize(json_encode($_confirmations));
                                            }
                                            if ($giftQuantity > 0) {
                                                self::logtxt("ENTRO A AGREGAR REGALO: " . ($key + 1) . "");
                                                self::addCartRuleGift($plan_name, $id_product, $priority, $giftQuantity, $result->cardnumber, $result->transactionid, $this->context);
                                                $priority += $giftQuantity;
                                                self::logtxt("ENTRO A AGREGAR REGALO nro de regalos: " . ($key + 1) . " de : " . $giftQuantity);
                                                foreach ($products as $key => $product_msg) {
                                                    if ($product_msg['reference'] == $reference) {
                                                        $product_name = $product_msg['name'];
                                                        break;
                                                    }
                                                }
                                                $_confirmations[] = htmlspecialchars($plan['description'] . ", te obsequia " . $giftQuantity . " " . $product_name . " como beneficio de afiliado.");
                                                $this->context->cookie->{'_confirmations_' . $plan_name} = serialize(json_encode($_confirmations));
                                            }

                                            Db::getInstance()->insert('orbisfarma_transaction_quote', array(
                                                'id_orbisfarma_service' => $id_orbisfarma_service,
                                                'id_cart' => $cart->id,
                                                'id_product' => $id_product,
                                                'transactionid' => $result->transactionid,
                                                'reference' => $reference,
                                                'discount_quantity' => $discountQuantity,
                                                'discount' => $discount,
                                                'gift_quantity' => $giftQuantity,
                                                'transactiondate' => $result->transactiondate
                                                    )
                                                    , false, true, Db::REPLACE);
                                        } else {
                                            self::logtxt("No se pudo agregar un regalo ya que el producto de regalo o descuento no existe " . ($key + 1) . ".");
                                        }
                                    }
                                }
                            } else {
                                $_errors_{$plan_name}[] = htmlspecialchars($result->message);
                                $this->context->cookie->{'_errors_' . $plan_name} = serialize(json_encode($_errors_{$plan_name}));
                            }
                        } else {
                            self::logtxt("Matching products do not existe.");
                        }
                    } else {
                        self::logtxt("There is no session ID.");
                        return null;
                    }
                    $this->context->cookie->{'quote_' . strtolower($plan_name)} = true;
                } else {
                    $this->context->cookie->{'quote_' . strtolower($plan_name)} = false;
                }
            } else {
                $this->context->cookie->{'quote_' . strtolower($plan_name)} = true;
            }
        }
    }

    /**
     * Set Transaction Sale
     * 
     * @param Order $order
     * @return array() response transaction sale
     */
    public function setTransactionSale($order, $plan) {

        self::logtxt("FUNCTION setTransactionSale()");
        $id_orbisfarma_service = $plan['id_orbisfarma_service'];
        $plan_name = $plan['name'];

        $query = new DbQuery();
        $query->select("od.product_reference, od.product_quantity, od.unit_price_tax_incl, od.total_price_tax_incl, ifnull(discount,0) discount, ifnull(gift_quantity,0) gift_quantity");
        $query->from("orders", "o");
        $query->leftJoin("order_detail", "od", "(od.id_order = o.id_order)");
        $query->leftJoin("orbisfarma_transaction_quote", "ocd", "(ocd.id_cart = o.id_cart AND ocd.id_product = od.product_id)");
        $query->where("o.id_cart = " . (int) $order->id_cart);
        $query->where("ocd.id_orbisfarma_service = " . (int) $id_orbisfarma_service);
        $sql = $query->build();

        if ($products = Db::getInstance()->executeS($query)) {

            self::logtxt("List of the products order exist");
            $productsOrigisFormat = array();

            foreach ($products as $key => $product) {
                $productsOrigisFormat[] = implode(',', array($product['product_reference'], $product['product_quantity'], round($product['unit_price_tax_incl'] * (1 - $product['discount'] / 100), 2), round($product['total_price_tax_incl'] * (1 - $product['discount'] / 100), 2)));
            }
            $transactionitems = implode('|', $productsOrigisFormat);
            $config = self::getConfigurationOrbis($plan['name']);

            $parameters = array(
                'cardnumber' => self::getCustomerCard($id_orbisfarma_service, $order->id_customer),
                'storeid' => $config['storeid'],
                'posid' => $config['posid'],
                'employeeid' => $config['employeeid'],
                'transactionid' => self::getTransactionID($order->id_cart, $id_orbisfarma_service),
                'transactionitems' => $transactionitems,
                'transactionwithdrawal' => 0,
                'invoicenumber' => $order->id,
                'invoicedate' => $order->date_add,
                'invoiceamount' => $order->total_paid,
                'key' => self::getKeyWS($plan_name)
            );
            try {
                $server = self::getWSDL($plan_name);
                $client = new SoapClient($server);
                self::logtxt("SERVER: " . $server);
                self::logtxt("Request setTransactionSale : id_orden: " . $order->id . ": " . json_encode($parameters));
                $response = $client->setTransactionSale($parameters);
                $response2 = simplexml_load_string($response->setTransactionSaleResult->any);
                $result = $response2->NewDataSet->FirstTable;
                self::logtxt("Result setTransactionSale : id_orden: " . $order->id . ": " . json_encode($result));
            } catch (SoapFault $fault) {
                self::logtxt("[SoapFault " . $fault->faultcode . "] setTransactionSale " . $fault->faultstring . ', el servicio no responde en este momento, por favor comuniquese a mesa de ayuda Orbisfarma ó inténtelo más tarde.');
                $_errors_{$plan_name}[] = '[SoapFault ' . $fault->faultcode . ' setTransactionSale] El servicio no responde en este momento, por favor comuniquese a mesa de ayuda Orbisfarma ó inténtelo más tarde.';
                $this->context->cookie->{'_errors_' . $plan_name} = serialize(json_encode($_errors_{$plan_name}));
                return;
            }

            if ($result->errorid == 0) {

                $discounts = explode('|', $result->transactionitems);
                $priceDiscount = array();
                $priority = 1;

                foreach ($discounts as $key => $discount) {
                    self::logtxt("setTransactionSale KEY " . $key . ":  ");

                    if (!empty($discount)) {
                        $discountArray = explode(',', $discount);
                        $reference = $discountArray[0];
                        $discountQuantity = $discountArray[1];
                        $id_product = self::getIdByReference($reference);

                        if (isset($id_product) && !empty($id_product)) {

                            $discount = $discountArray[2];
                            $giftQuantity = $discountArray[3];
                            $points = $discountArray[4];

                            if (Db::getInstance()->insert('orbisfarma_transaction_sale', array(
                                        'id_order' => (int) $order->id,
                                        'id_cart' => (int) $order->id_cart,
                                        'transactionid' => pSQL($result->transactionid),
                                        'saleauthnumber' => pSQL($result->saleauthnumber),
                                        'cardnumber' => pSQL($result->cardnumber),
                                        'cardbalance' => pSQL($result->cardbalance),
                                        'invoicemessage' => pSQL($result->invoicemessage),
                                        'errorid' => (int) $result->errorid,
                                        'id_product' => (int) $id_product,
                                        'reference' => pSQL($reference),
                                        'discount_quantity' => (int) $discountQuantity,
                                        'gift_quantity' => (int) $giftQuantity,
                                        'discount' => (int) $discount,
                                        'points' => (int) $points,
                                        'transactiondate' => pSQL($result->transactiondate),
                                        'date_add' => date('Y:m:d H:i:s')))) {

                                self::logtxt("REGISTRO orbisfarma_transaction_sale:  ");
                            } else {

                                self::logtxt("NO REGISTRO orbisfarma_transaction_sale:  ");
                            }
                        }
                    }
                }
            } else if ($result->errorid != 0) {
                $this->_errors[] = $result->message;
            }
        } else {
            self::logtxt("setTransactionSale() There are no products with benefits Origis");
            return false;
        }

        return $result;
    }

    /**
     * Set Sale Reverse
     * 
     * @param Order $order
     * @return int $errorid Response web service
     */
    public function setSaleReverse($order, $plan) {

        self::logtxt("FUNCTION setSaleReverse()");

        $id_orbisfarma_service = $plan['id_orbisfarma_service'];
        $plan_name = $plan['name'];
        $config = self::getConfigurationOrbis($plan_name);
        $server = self::getWSDL($plan_name);
        $client = new SoapClient($server);
        self::logtxt("SERVER: " . $server);

        $parameters = array(
            'cardnumber' => self::getCustomerCard($id_orbisfarma_service, $order->id_customer),
            'storeid' => $config['storeid'],
            'posid' => $config['posid'],
            'employeeid' => $config['employeeid'],
            'transactionid' => self::getTransactionID($order->id_cart, $id_orbisfarma_service),
            'saleauthnumber' => self::getSaleauthnumber($order->id, $order->id_cart),
            'invoicenumber' => strval($order->id),
            'Key' => self::getKeyWS($plan_name)
        );

        self::logtxt("Request setSaleReverse : id_orden: " . $order->id . ": " . json_encode($parameters));
        $response = $client->setSaleReverse($parameters);
        $response2 = simplexml_load_string($response->setSaleReverseResult->any);
        $result = $response2->NewDataSet->FirstTable;
        self::logtxt("Result setSaleReverse : id_orden: " . $order->id . ": " . json_encode($result));

        if (Db::getInstance()->insert('orbisfarma_sale_reverse', array(
                    'transactionid' => pSQL($result->transactionid),
                    'saleauthnumber' => pSQL($result->saleauthnumber),
                    'cardnumber' => pSQL($result->cardnumber),
                    'errorid' => (int) $result->errorid,
                    'message' => pSQL($result->message),
                    'transactiondate' => pSQL($result->transactiondate),
                    'date_add' => date('Y:m:d H:i:s')))) {

            self::logtxt("RESGISTRO orbisfarma_sale_reverse:  ");
        } else {

            self::logtxt("NO REGISTRO GUARDO orbisfarma_sale_reverse:  ");
        }

        if ($result->errorid != 0) {
            $this->_errors[] = $result->message;
        }
        return $result;
    }

    /**
     * Get Customer Card
     * 
     * @param type $customerid
     * @return boolean
     */
    public static function getCustomerCard($id_orbisfarma_service, $id_customer = null) {

        if (empty($id_customer)) {
            $id_customer = Context::getContext()->customer->id;
        }
        if (!empty($id_customer)) {
            $query = new DbQuery();
            $query->select("number");
            $query->from("orbisfarma_customer_card");
            $query->where("id_orbisfarma_service =  " . (int) $id_orbisfarma_service);
            $query->where("id_customer =  " . (int) $id_customer);

            if ($result = Db::getInstance()->getValue($query))
                return $result;
            else
                return false;
        } else {
            return false;
        }
    }

    /**
     * Get Register Card
     * 
     * @param type $customerid
     * @return boolean
     */
    public static function getRegisterCard($id_orbisfarma_service, $id_customer = null) {

        if (empty($id_customer)) {
            $id_customer = Context::getContext()->customer->id;
        }
        if (!empty($id_customer)) {
            $query = new DbQuery();
            $query->select("*");
            $query->from("orbisfarma_customer_card");
            $query->where("id_orbisfarma_service =  " . (int) $id_orbisfarma_service);
            $query->where("id_customer =  " . (int) $id_customer);

            if ($result = Db::getInstance()->executeS($query))
                return true;
            else
                return false;
        } else {
            return false;
        }
    }

    /**
     * Get Transaction ID
     * 
     * @param type $cartid
     * @return boolean
     */
    public static function getTransactionID($id_cart, $id_orbisfarma_service) {

        if (!empty($id_cart)) {

            $query = new DbQuery();
            $query->select("transactionid");
            $query->from("orbisfarma_transaction_quote");
            $query->where("id_cart = " . (int) $id_cart);
            $query->where("id_orbisfarma_service = " . (int) $id_orbisfarma_service);

            if ($result = Db::getInstance()->getValue($query))
                return $result;
            else
                return false;
        }

        return false;
    }

    /**
     * 
     * @param type $id_order
     * @param type $id_cart
     */
    public static function getSaleauthnumber($id_order, $id_cart = null) {

        if (!$id_cart) {
            $context = Context::getContext();
            $id_cart = $context->cart->id;
        }
        if (!empty($id_cart)) {
            $query = new DbQuery();
            $query->select("saleauthnumber");
            $query->from("orbisfarma_transaction_sale");
            $query->where("id_order = " . $id_order);
            $query->where("id_cart = " . $id_cart);
            $query->where("errorid = 0");

            if ($result = Db::getInstance()->getValue($query))
                return $result;
            else
                return null;
        }
    }

    /**
     * Error log
     * 
     * @param string $text text that will be saved in the file  
     * @return void Error record in file "log_errors.log"
     */
    public static function logtxt($text = "") {
        if (file_exists(self::ORBISFARMA_PATH_LOG)) {

            $fp = fopen(self::ORBISFARMA_PATH_LOG . "/errors.log", "a+");
            fwrite($fp, date('l jS \of F Y h:i:s A') . ", " . $text . "\r\n");
            fclose($fp);
            return true;
        } else {
            self::createPath(self::ORBISFARMA_PATH_LOG);
        }
    }

    /**
     * Recursively create a string of directories
     */
    public static function createPath($path) {

        if (is_dir($path))
            return true;

        $prev_path = substr($path, 0, strrpos($path, '/', -2) + 1);
        $return = self::createPath($prev_path);
        return ($return && is_writable($prev_path)) ? mkdir($path) : false;
    }

    /**
     * Get Environment
     * @return int 1 if production environment or 0 if test environment
     */
    public static function getEnvironment($plan_name) {

        $sql = "SELECT value FROM " . _DB_PREFIX_ . "configuration WHERE name = 'ORBISFARMA_PRODUCTION_ENVIRONMENT_" . strtoupper($plan_name) . "'";

        return (int) Db::getInstance()->getValue($sql);
    }

    /**
     * Get the url of the web service according to the development environment
     * @return string url web service
     */
    private static function getWSDL($plan_name) {

        $environment = self::getEnvironment($plan_name);
        if ($environment) {
            return Configuration::get('ORBISFARMA_WSDL_' . strtoupper($plan_name));
        } else {
            return Configuration::get('ORBISFARMA_WSDL_SANDBOX_' . strtoupper($plan_name));
        }
    }

    /**
     * Get the Key of the web service according to the development environment
     * @return string Key web service
     */
    private static function getKeyWS($plan_name) {

        $environment = self::getEnvironment($plan_name);

        if ($environment) {
            return Configuration::get('ORBISFARMA_KEY_' . strtoupper($plan_name));
        } else {
            return Configuration::get('ORBISFARMA_KEY_SANDBOX_' . strtoupper($plan_name));
        }
    }

    /**
     * Get the Key of the web service according to the development environment
     * @return string Key web service
     */
    private static function getConfigurationOrbis($plan_name) {

        return array(
            'storeid' => Configuration::get('ORBISFARMA_STOREID_' . strtoupper($plan_name)),
            'posid' => Configuration::get('ORBISFARMA_POSID_' . strtoupper($plan_name)),
            'employeeid' => Configuration::get('ORBISFARMA_EMPLOYEEID_' . strtoupper($plan_name)),
        );
    }

    /**
     * Add new rule of group at the cart
     * 
     * @param int $id_product
     * @param int $discountQuantity
     * @param int $reduction_percent
     * @param int $priority
     * @return void Add new rule of group at the cart
     */
    private static function addCartRuleDiscount($name, $id_product, $discountQuantity, $reduction_percent, $priority, $cardnumber, $transactionid, $context) {

        self::logtxt("DESCUENTO " . $name);
        self::logtxt("Id_product: " . $id_product);
        self::logtxt("DiscountQuantity: " . $discountQuantity);
        self::logtxt("Reduction_percent: " . $reduction_percent);
        self::logtxt("Priority: " . $priority);
        self::logtxt("Cardnumber: " . $cardnumber);
        self::logtxt("Transactionid: " . $transactionid);

        $cartRuleObj = new CartRule();
        $cartRuleObj->name[Configuration::get('PS_LANG_DEFAULT')] = strtoupper($name) . '-' . $context->customer->id . '-T' . $cardnumber . '/TID' . $transactionid;
        $cartRuleObj->id_customer = $context->customer->id;
        $cartRuleObj->date_from = date('Y-m-d H:i:s');
        $cartRuleObj->date_to = date('Y-m-d H:i:s', (strtotime("+2 Hours")));   //    add 2 hours from the current time
        $cartRuleObj->description = Configuration::get('ORBISFARMA_MSM_DISC');
        $cartRuleObj->quantity = $discountQuantity;
        $cartRuleObj->quantity_per_user = 1;
        $cartRuleObj->priority = $priority;
        $cartRuleObj->minimum_amount = 0;
        $cartRuleObj->minimum_amount_tax = 0;
        $cartRuleObj->minimum_amount_shipping = 0;
        $cartRuleObj->product_restriction = 1;
        $cartRuleObj->free_shipping = 1;
        $cartRuleObj->reduction_percent = $reduction_percent;
        $cartRuleObj->reduction_currency = 0;
        $cartRuleObj->reduction_exclude_special = 0;
        $cartRuleObj->reduction_tax = 0;
        $cartRuleObj->reduction_product = (int) $id_product;
        $cartRuleObj->highlight = 1;
        $cartRuleObj->active = 1;
        $cartRuleObj->partial_use = 0;
        $cartRuleObj->country_restriction = 0;

        if ($cartRuleObj->add()) {
            Db::getInstance()->insert('cart_rule_shop',
                    array('id_cart_rule' => (int) $cartRuleObj->id,
                        'id_shop' => (int) $context->shop->id),
                    false, false, Db::REPLACE);

            Db::getInstance()->insert('cart_rule_lang',
                    array('id_cart_rule' => (int) $cartRuleObj->id,
                        'id_lang' => (int) $context->language->id,
                        'name' => pSQL($cartRuleObj->name[Configuration::get('PS_LANG_DEFAULT')])),
                    false, false, Db::REPLACE);

            Db::getInstance()->insert('cart_rule_country',
                    array('id_cart_rule' => (int) $cartRuleObj->id,
                        'id_country' => (int) Configuration::get('PS_COUNTRY_DEFAULT')),
                    false, false, Db::INSERT);

            Db::getInstance()->insert('cart_rule_product_rule_group',
                    array('id_product_rule_group' => null,
                        'id_cart_rule' => (int) $cartRuleObj->id,
                        'quantity' => (int) 1),
                    false, false, Db::INSERT);

            $product_rule_group_id = Db::getInstance()->Insert_ID();

            Db::getInstance()->insert('cart_rule_product_rule',
                    array('id_product_rule_group' => (int) $product_rule_group_id,
                        'type' => 'products'),
                    false, false, Db::INSERT);

            $product_rule_id = Db::getInstance()->Insert_ID();

            Db::getInstance()->insert('cart_rule_product_rule_value',
                    array('id_product_rule' => (int) $product_rule_id,
                        'id_item' => (int) $id_product),
                    false, false, Db::INSERT);

            self::logtxt("NAME RULE:" . $cartRuleObj->name[Configuration::get('PS_LANG_DEFAULT')]);

            $context->cart->addCartRule($cartRuleObj->id);
            self::logtxt("PRIORITY : " . $priority);
            $priority++;
        } else {
            self::logtxt("NO Agrego nueva regla de DESCUENTO, id_customer: " . $context->customer->id . ", id_product: " . $id_product . ", id_cart: " . $context->cart->id);
        }
    }

    /**
     * Add new rule of group at the cart
     * @param int $id_product
     * @param int $priority
     * @param int $giftQuantity
     * @return void Add new rule of group at the cart
     */
    private static function addCartRuleGift($name, $id_product, $priority, $giftQuantity, $cardnumber, $transactionid, $context) {

        self::logtxt("REGALO " . $name);
        self::logtxt("Id_product: " . $id_product);
        self::logtxt("Priority: " . $priority);
        self::logtxt("GiftQuantity: " . $giftQuantity);
        self::logtxt("Cardnumber: " . $cardnumber);
        self::logtxt("Transactionid: " . $transactionid);

        for ($i = 0; $i < $giftQuantity; $i++) {

            $cartRuleObj = new CartRule();
            $cartRuleObj->name[Configuration::get('PS_LANG_DEFAULT')] = strtoupper($name) . '-' . $context->customer->id . '-' . $priority . '-T' . $cardnumber . '/TID' . $transactionid;
            $cartRuleObj->id_customer = $context->customer->id;
            $cartRuleObj->date_from = date('Y-m-d H:i:s');
            $cartRuleObj->date_to = date('Y-m-d H:i:s', (strtotime("+2 Hours")));   //    add 2 hours from the current time
            $cartRuleObj->description = Configuration::get('ORBISFARMA_MSM_GIFT');
            $cartRuleObj->quantity = 1;
            $cartRuleObj->quantity_per_user = 1;
            $cartRuleObj->priority = $priority;
            $cartRuleObj->minimum_amount = 0;
            $cartRuleObj->minimum_amount_tax = 0;
            $cartRuleObj->minimum_amount_shipping = 0;
            $cartRuleObj->product_restriction = 0;
            $cartRuleObj->reduction_currency = 0;
            $cartRuleObj->reduction_tax = 0;
            $cartRuleObj->gift_product = (int) $id_product;
            $cartRuleObj->highlight = 1;
            $cartRuleObj->active = 1;
            $cartRuleObj->partial_use = 0;
            $cartRuleObj->country_restriction = 0;

            if ($cartRuleObj->add()) {
                Db::getInstance()->insert('cart_rule_shop',
                        array('id_cart_rule' => (int) $cartRuleObj->id,
                            'id_shop' => (int) $context->shop->id),
                        false, false, Db::REPLACE);

                Db::getInstance()->insert('cart_rule_lang',
                        array('id_cart_rule' => (int) $cartRuleObj->id,
                            'id_lang' => (int) $context->language->id,
                            'name' => pSQL($cartRuleObj->name[Configuration::get('PS_LANG_DEFAULT')])),
                        false, false, Db::REPLACE);

                Db::getInstance()->insert('cart_rule_country', array('id_cart_rule' => (int) $cartRuleObj->id,
                    'id_country' => (int) Configuration::get('PS_COUNTRY_DEFAULT')), false, false, Db::INSERT);


                self::logtxt("Rule created: " . $cartRuleObj->name[Configuration::get('PS_LANG_DEFAULT')]);

                $context->cart->addCartRule($cartRuleObj->id);
                self::logtxt("->PRIORITY : " . $priority);
                $priority++;
            } else {
                self::logtxt("->NO Agrego nueva regla de Regalo, id_customer: " . $context->customer->id . ", id_product: " . $id_product . ", id_cart: " . $context->cart->id);
            }
        }
    }

    /**
     * Get Id By Reference
     *
     * @param string $reference Id of the product reference
     * @return int Id of product
     */
    private static function getIdByReference($reference) {

        $context = Context::getContext();

        $query = new DbQuery();
        $query->select("p.id_product, p.reference, pl.name");
        $query->from("product", 'p');
        $query->leftJoin('product_lang', 'pl', '(p.id_product = pl.id_product)');
        $query->where("p.reference = " . pSQL($reference));
        $query->where("pl.id_lang = " . (int) $context->language->id);

        if ($results = Db::getInstance()->ExecuteS($query)) {

            return isset($results[0]['id_product']) ? $results[0]['id_product'] : null;
        } else
            return null;
    }

    /**
     * Removing the rules to make a new quote
     *
     * @return boolean true if removed rules
     */
    private static function removeRules($context, $plan_name) {

        self::logtxt("-------------_> ENTRO removeRules().");
        $cart_rules = $context->cart->getCartRules(CartRule::FILTER_ACTION_ALL, false);
        $removed = false;
        $name_rule = '/^' . strtoupper(Configuration::get('ORBISFARMA_NAME_' . strtoupper($plan_name))) . '-' . $context->customer->id . '-/';

        self::logtxt("-------------_> Name Rule: " . $name_rule);
        foreach ($cart_rules as $key => $cart_rule) {
            CartRule::cleanCache();
            if (($rule = new CartRule((int) $cart_rule['obj']->id)) && Validate::isLoadedObject($rule)) {
                if ($error = $rule->checkValidity($context, false, true)) {
                    if (preg_match($name_rule, $cart_rule['obj']->name)) {
                        $rule->active = false;
                        if ($rule->update()) {
                            if ($context->cart->removeCartRule((int) $rule->id)) {
                                if (!$rule->delete()) {
                                    self::logtxt("Rule NOT deleted: " . $cart_rule['obj']->name);
                                    $removed = false;
                                } else {
                                    self::logtxt("Rule deleted: " . $cart_rule['obj']->name);
                                    $removed = true;
                                }
                            }
                        }
                    }
                } else {
                    self::logtxt("No pudo checkValidity().");
                }
            } else {
                self::logtxt("No pudo instanciar la regla de carrito ni cargar el objeto.");
            }
        }
        if (isset($context->cookie->{'_confirmations_' . $plan_name}))
            unset($context->cookie->{'_confirmations_' . $plan_name});
        if (isset($context->cookie->{'_errors_' . $plan_name}))
            unset($context->cookie->{'_errors_' . $plan_name});
        return $removed;
    }

    /**
     * Validate if this use is patient of the plan
     *
     * @return boolean True if this use is patient of the plan
     */
    private static function isCustomerPlan($id_orbisfarma_service, $id_customer) {

        $query = new DbQuery();
        $query->select("number");
        $query->from("orbisfarma_customer_card");
        $query->where("id_customer = " . (int) $id_customer);
        $query->where("id_orbisfarma_service = " . (int) $id_orbisfarma_service);
        $q = $query->build();
        $results = Db::getInstance()->getValue($query);
        if ($results === null || !$results) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Get arrray with items return of the NovoNordisk services
     * @param type $id_order
     * @return type
     */
    public function getArrayItems($carditems) {

        $cadena = str_replace(array('0:', '|1:|'), "", $carditems);
        return preg_split("/[,]+/", $cadena);
    }

    public static function isActiveOrbisfarmaService($id_orbisfarma_service) {

        $sql = new DbQuery();
        $sql->select('active');
        $sql->from('orbisfarma_service', 'o');
        $sql->where("o.id_orbisfarma_service = " . (int) $id_orbisfarma_service);
        $e = $sql->build();
        return (bool) Db::getInstance()->getValue($sql);
    }

    public static function getPlans() {

        $sql = new DbQuery();
        $sql->select('*');
        $sql->from('orbisfarma_service', 'o');
        $sql->where('o.active = 1');
        return Db::getInstance()->executeS($sql);
    }

    public static function getPlanById($id_orbisfarma_service) {

        $sql = new DbQuery();
        $sql->select('*');
        $sql->from('orbisfarma_service', 'o');
        $sql->where('o.id_orbisfarma_service = ' . (int) $id_orbisfarma_service);
        $sql->limit(1);
        return Db::getInstance()->executeS($sql)[0];
    }

    public static function getAllsPlans() {

        $sql = new DbQuery();
        $sql->select('*');
        $sql->from('orbisfarma_service', 'o');
        return Db::getInstance()->executeS($sql);
    }

    public static function getPlansCustomerDisabled($id_customer) {
        $sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'orbisfarma_service` os
	WHERE NOT EXISTS (SELECT NULL FROM  `' . _DB_PREFIX_ . 'orbisfarma_customer_card` occ'
                . ' WHERE occ.id_orbisfarma_service = os.id_orbisfarma_service AND occ.id_customer = ' . (int) $id_customer . ' AND occ.number != "")'
                . ' AND os.active = 1'
                . ' ORDER BY os.description';
        return Db::getInstance()->executeS($sql);
    }

    public static function getPlansCustomerEnabled($id_customer) {

        $sql = new DbQuery();
        $sql->select('*');
        $sql->from('orbisfarma_service', 'os');
        $sql->leftJoin('orbisfarma_customer_card', 'occ', '(os.id_orbisfarma_service = occ.id_orbisfarma_service)');
        $sql->where('os.active = 1');
        $sql->where('occ.id_customer = ' . (int) $id_customer);
        $sql->orderBy('os.description');
        return Db::getInstance()->executeS($sql);
    }

    public static function cartExists($card) {

        $sql = new DbQuery();
        $sql->select('*');
        $sql->from('orbisfarma_customer_card', 'cc');
        $sql->where('cc.number = ' . (int) $card);
        return Db::getInstance()->executeS($sql);
    }

}
<?php

class AdminCartsController extends AdminCartsControllerCore {

    public function init() {

        $moduleManagerBuilder = PrestaShop\PrestaShop\Core\Addon\Module\ModuleManagerBuilder::getInstance();
        $moduleManager = $moduleManagerBuilder->build();
        if ($moduleManager->isInstalled('orbisfarma') && $moduleManager->isEnabled('orbisfarma')) {
            $orbisfarma = Module::getInstanceByName('orbisfarma');
            foreach ($orbisfarma::getPlans() as $plan) {
                $this->context->cookie->{'quote_' . strtolower($plan['name'])} = false;
            }
        }
        parent::init();
    }

    public function initContent() {
        if (Configuration::isCatalogMode() && Tools::getValue('action') === 'show') {
            Tools::redirect('index.php');
        }

        $moduleManagerBuilder = PrestaShop\PrestaShop\Core\Addon\Module\ModuleManagerBuilder::getInstance();
        $moduleManager = $moduleManagerBuilder->build();
        if ($moduleManager->isInstalled('orbisfarma') && $moduleManager->isEnabled('orbisfarma')) {
            Hook::exec('actionUpdatediscountsOrbisfarma', array('cart' => $this->context->cart), Module::getModuleIdByName('orbisfarma'));
        }

        parent::initContent();
    }

}

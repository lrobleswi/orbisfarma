<?php

class CartController extends CartControllerCore {

    public function init() {

        $moduleManagerBuilder = PrestaShop\PrestaShop\Core\Addon\Module\ModuleManagerBuilder::getInstance();
        $moduleManager = $moduleManagerBuilder->build();
        if ($moduleManager->isInstalled('orbisfarma') && $moduleManager->isEnabled('orbisfarma')) {
            $orbisfarma = Module::getInstanceByName('orbisfarma');
            foreach ($orbisfarma::getPlans() as $plan) {
                if ($this->context->customer->isLogged() && Tools::isSubmit("action") && Tools::getValue("action") != "refresh") {
                    $this->context->cookie->{'quote_' . strtolower($plan['name'])} = false;
                }
            }
        }
        parent::init();
    }

    public function initContent() {
        if (Configuration::isCatalogMode() && Tools::getValue('action') === 'show') {
            Tools::redirect('index.php');
        }

        $moduleManagerBuilder = PrestaShop\PrestaShop\Core\Addon\Module\ModuleManagerBuilder::getInstance();
        $moduleManager = $moduleManagerBuilder->build();
        if ($moduleManager->isInstalled('orbisfarma') && $moduleManager->isEnabled('orbisfarma')) {
            Hook::exec('actionUpdatediscountsOrbisfarma', array('cart' => $this->context->cart), Module::getModuleIdByName('orbisfarma'));
        }

        parent::initContent();
    }

    public function displayAjaxUpdate() {
        if (Configuration::isCatalogMode()) {
            return;
        }

        if (!$this->errors) {

            $this->ajaxDie(Tools::jsonEncode([
                        'success' => true,
                        'id_product' => $this->id_product,
                        'id_product_attribute' => $this->id_product_attribute,
                        'quantity' => null,
                        'cart' => null,
                        'errors' => empty($this->updateOperationError) ? '' : reset($this->updateOperationError),
            ]));
        } else {
            $this->ajaxDie(Tools::jsonEncode([
                        'hasError' => true,
                        'errors' => $this->errors,
                        'quantity' => null,
            ]));
        }
    }

}

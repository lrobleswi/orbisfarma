<?php

/**
 *  2019-2020 FARMALISTO
 *
 *  @author    Jhonny Romero <jromeroarenales@gmail.com>
 *  @copyright 2017-2020 FARMALISTO
 *  @license   https://www.farmalisto.com.mx/ - prestashop module orbisfarma
 */
if (!defined('_PS_VERSION_'))
    exit;

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class Pk_CustomLinksOverride extends Pk_CustomLinks implements WidgetInterface {

    public function renderWidget($hookName, array $configuration) {
        if ($this->context->language->iso_code == 'mx') {
            $moduleManagerBuilder = PrestaShop\PrestaShop\Core\Addon\Module\ModuleManagerBuilder::getInstance();
            $moduleManager = $moduleManagerBuilder->build();
            if ($moduleManager->isInstalled('orbisfarma') && $moduleManager->isEnabled('orbisfarma')) {
                $orbisfarma = Module::getInstanceByName('orbisfarma');
                $this->smarty->assign(array(
                    'plans' => $orbisfarma::getPlans()
                ));
                $this->templateFile = 'module:orbisfarma/views/templates/modules/pk_customlinks/pk_customlinks.tpl';
            }
        }
        return parent::renderWidget($hookName, $configuration);
    }

}
<?php

/**
 *  2019-2020 FARMALISTO
 *
 *  @author    Jhonny Romero <jromeroarenales@gmail.com>
 *  @copyright 2017-2020 FARMALISTO
 *  @license   https://www.farmalisto.com.mx/ - prestashop module orbisfarma
 */
class Validate extends ValidateCore
{
    public static function isNumericOnly($iso_code)
    {
        return preg_match('/^[0-9]*[1-9][0-9]*$/', $iso_code);
    }
}

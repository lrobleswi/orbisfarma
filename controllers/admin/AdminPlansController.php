<?php

/**
 *  2019-2020 FARMALISTO
 *
 *  @author    Jhonny Romero <jromeroarenales@gmail.com>
 *  @copyright 2017-2020 FARMALISTO
 *  @license   https://www.farmalisto.com.mx/ - prestashop module orbisfarma
 */
defined('_PS_VERSION_') or exit;
include_once (_PS_MODULE_DIR_ . 'orbisfarma/classes/OrbisfarmaService.php');

class AdminPlansController extends ModuleAdminController {

    protected $can_add_plan = true;
    protected $restrict_edition = false;

    /** @var array tabs list */
    protected $tabs_list = array();

    public function __construct() {
        $this->bootstrap = true;
        $this->required_database = false;
        $this->table = 'orbisfarma_service';
        $this->className = 'OrbisfarmaService';
        $this->explicitSelect = false;
        $this->lang = false;

        parent::__construct();

        $this->addRowAction('delete');
        $this->addRowAction('edit');
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->trans('Delete selected', array(), 'Admin.Notifications.Info'),
                'confirm' => $this->trans('Delete selected items?', array(), 'Admin.Notifications.Info'),
                'icon' => 'icon-trash'
            )
        );

        $this->fields_list = array(
            'id_orbisfarma_service' => array(
                'title' => $this->l('ID Plan'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
            ),
            'name' => array(
                'title' => $this->trans('Name', array(), 'Admin.Global'),
                'align' => 'center',
            ),
            'description' => array(
                'title' => $this->trans('Description', array(), 'Admin.Global'),
                'orderby' => false
            ),
            'key' => array(
                'title' => $this->l('Key'),
                'class' => 'fixed-width-md'
            ),
            'key_sandbox' => array(
                'title' => $this->l('Key Sandbox'),
                'class' => 'fixed-width-md'
            ),
            'active' => array(
                'title' => $this->trans('Active', array(), 'Admin.Global'),
                'active' => 'status',
                'type' => 'bool',
                'class' => 'fixed-width-xs',
                'align' => 'center',
                'ajax' => true,
                'orderby' => false
            ),
            'date_add' => array(
                'title' => $this->trans('Date', array(), 'Admin.Global'),
                'align' => 'center',
                'type' => 'datetime',
                'class' => 'fixed-width-xl',
                'filter_key' => 'a!date_add',
            ),
        );
        // Check if we can add a customer
        if (Shop::isFeatureActive() && (Shop::getContext() == Shop::CONTEXT_ALL || Shop::getContext() == Shop::CONTEXT_GROUP)) {
            $this->can_add_plan = false;
        }
    }

    /**
     * Fetch the template for action enable
     *
     * @param string $token
     * @param string $id
     * @param int $value state enabled or not
     * @param string $active status
     * @param int $id_category
     * @param int $id_product
     * @return string
     */
//    public function displayEnableLink($token, $id, $value, $active, $id_category = null, $id_product = null, $ajax = false) {
//
//        $tpl_enable = $this->context->smarty->createTemplate('helpers/list/list_action_enable.tpl');
//        $tpl_enable->assign(array(
//            'ajax' => $ajax,
//            'enabled' => (bool)$value,
//            'url_enable' => self::$currentIndex . '&' . $this->identifier . '=' . $id . '&' . $active . $this->table . ($ajax ? '&action=' . $active . $this->table . '&ajax=' . (int) $ajax : '') . '&token=' . ($token != null ? $token : $this->token)
//        ));
//        return $tpl_enable->fetch();
//    }

    public function renderForm() {
        /** @var Employee $obj */
        if (!($obj = $this->loadObject(true))) {
            return;
        }

        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Orbisfarma plan'),
                'icon' => 'icon-plus-sign-alt',
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->trans('Name', array(), 'Admin.Global'),
                    'name' => 'name',
                    'col' => '4',
                    'required' => true,
                    'hint' => $this->l('Name for the plan that is lowercase and without blank spaces.'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('Description', array(), 'Admin.Global'),
                    'name' => 'description',
                    'col' => '4',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('WSDL url'),
                    'name' => 'wsdl',
                    'col' => '4',
                    'required' => true,
                    'hint' => $this->l('WSDL url provided by Orbisfarma for this plan.'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('WSDL url Sandbox'),
                    'name' => 'wsdl_sandbox',
                    'col' => '4',
                    'required' => true,
                    'hint' => $this->l('WSDL url provided by Orbisfarma for this plan in Sandbox.'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('Key', array(), 'Admin.Advparameters.Feature'),
                    'name' => 'key',
                    'col' => '4',
                    'required' => true,
                    'hint' => $this->l('Key provided by Orbisfarma for this plan.'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Key Sandbox'),
                    'name' => 'key_sandbox',
                    'col' => '4',
                    'required' => true,
                    'hint' => $this->l('Key provided by Orbisfarma for this plan in Sandbox.'),
                )
            ),
        );

        $this->fields_form['submit'] = array(
            'title' => $this->trans('Save', array(), 'Admin.Global'),
        );

        if (empty($obj->id)) {
            $this->fields_value['id_lang'] = $this->context->language->id;
        }
//        $this->context->controller->addJS(_PS_MODULE_DIR_ . 'mastermama/views/js/back.js');

        return parent::renderForm();
    }

    public function init() {
        if (Tools::isSubmit('addorder_return_state')) {
            $this->display = 'add';
        }
        if (Tools::isSubmit('updateorder_return_state')) {
            $this->display = 'edit';
        }

        return parent::init();
    }

    /**
     * 
     */
    public function initPageHeaderToolbar() {
        parent::initPageHeaderToolbar();
        if (empty($this->display) && $this->can_add_plan) {
            $this->page_header_toolbar_btn['new_product'] = array(
                'href' => self::$currentIndex . '&addorbisfarma_service&token=' . $this->token,
                'desc' => $this->l('Add plan'),
                'icon' => 'process-icon-new'
            );
        }
    }

    /**
     * Call the right method for creating or updating object.
     *
     * @return ObjectModel|false|void
     */
    public function postProcess() {
        $object = parent::postProcess();
        if (isset($this->object) && ($this->object instanceof OrbisfarmaService) && isset($this->action) && $this->action != 'delete') {
            $name_plan = strtoupper($this->object->name);
            if (
                    !Configuration::updateValue('ORBISFARMA_WSDL_' . $name_plan, $this->object->wsdl) ||
                    !Configuration::updateValue('ORBISFARMA_WSDL_SANDBOX_' . $name_plan, $this->object->wsdl_sandbox) ||
                    !Configuration::updateValue('ORBISFARMA_ENABLED_' . $name_plan, $this->object->active) ||
                    !Configuration::updateValue('ORBISFARMA_PRODUCTION_ENVIRONMENT_' . $name_plan, 0) ||
                    !Configuration::updateValue('ORBISFARMA_NAME_' . $name_plan, $name_plan) ||
                    !Configuration::updateValue('ORBISFARMA_KEY_' . $name_plan, $this->object->key) ||
                    !Configuration::updateValue('ORBISFARMA_KEY_SANDBOX_' . $name_plan, $this->object->key_sandbox) ||
                    !Configuration::updateValue('ORBISFARMA_STOREID_' . $name_plan, 1) ||
                    !Configuration::updateValue('ORBISFARMA_POSID_' . $name_plan, 1) ||
                    !Configuration::updateValue('ORBISFARMA_EMPLOYEEID_' . $name_plan, 1) ||
                    !Configuration::updateValue('ORBISFARMA_MSM_DISC_' . $name_plan, 'Descuento aplicado en plan ' . $name_plan) ||
                    !Configuration::updateValue('ORBISFARMA_MSM_GIFT_' . $name_plan, 'Regalo aplicado en plan ' . $name_plan)
            ) {
                $this->errors[] = $this->l("An error occurrend while saving the configuration variables in the database.");
            }
        }
        return $object;
    }

    /**
     * Object Delete
     *
     * @return ObjectModel|false
     * @throws PrestaShopException
     */
    public function processDelete() {

        parent::processDelete();
        if (isset($this->object) && ($this->object instanceof OrbisfarmaService)) {
            $name_plan = strtoupper($this->object->name);
            if (
                    !Configuration::deleteByName('ORBISFARMA_WSDL_' . $name_plan) ||
                    !Configuration::deleteByName('ORBISFARMA_WSDL_SANDBOX_' . $name_plan) ||
                    !Configuration::deleteByName('ORBISFARMA_ENABLED_' . $name_plan) ||
                    !Configuration::deleteByName('ORBISFARMA_PRODUCTION_ENVIRONMENT_' . $name_plan) ||
                    !Configuration::deleteByName('ORBISFARMA_NAME_' . $name_plan) ||
                    !Configuration::deleteByName('ORBISFARMA_KEY_' . $name_plan) ||
                    !Configuration::deleteByName('ORBISFARMA_KEY_SANDBOX_' . $name_plan) ||
                    !Configuration::deleteByName('ORBISFARMA_STOREID_' . $name_plan) ||
                    !Configuration::deleteByName('ORBISFARMA_POSID_' . $name_plan) ||
                    !Configuration::deleteByName('ORBISFARMA_EMPLOYEEID_' . $name_plan) ||
                    !Configuration::deleteByName('ORBISFARMA_MSM_DISC_' . $name_plan) ||
                    !Configuration::deleteByName('ORBISFARMA_MSM_GIFT_' . $name_plan)
            ) {
                $this->errors[] = $this->l("An error occurrend while deleted the configuration variables in the database.");
            } else {
                $this->confirmations[] = $this->l("Configuration variables were deleted in the database correctly.");
            }
        }
    }

    public function ajaxProcessstatusorbisfarmaService() {
        if (Tools::isSubmit("id_orbisfarma_service")) {
            $id_orbisfarma_service = (int) Tools::getValue('id_orbisfarma_service');
            $sql = 'UPDATE ' . _DB_PREFIX_ . 'orbisfarma_service SET `active`= NOT `active` WHERE id_orbisfarma_service=' . $id_orbisfarma_service;
            $result = Db::getInstance()->execute($sql);
            if ($result) {
                if ($plan = orbisfarma::getPlanById($id_orbisfarma_service)) {
                    $name_plan = strtoupper($plan['name']);
                    if ($plan['active'] == 1) {
                        Configuration::updateValue('ORBISFARMA_ENABLED_' . $name_plan, '1');
                        exit(json_encode(array('success' => 1, 'text' => $this->trans('The status has been updated successfully.', array(), 'Admin.Notifications.Success'))));
                    } elseif ($plan['active'] == 0) {
                        Configuration::updateValue('ORBISFARMA_ENABLED_' . $name_plan, '0');
                        exit(json_encode(array('success' => 1, 'text' => $this->trans('The status has been updated successfully.', array(), 'Admin.Notifications.Success'))));
                    } else {
                        exit(json_encode(array('success' => 0, 'text' => $this->trans('An error occurred while updating the status.', array(), 'Admin.Notifications.Error'))));
                    }
                } else {
                    exit(json_encode(array('success' => 0, 'text' => $this->trans('An error occurred while updating the status.', array(), 'Admin.Notifications.Error'))));
                }
            } else {
                exit(json_encode(array('success' => 0, 'text' => $this->trans('An error occurred while updating the status.', array(), 'Admin.Notifications.Error'))));
            }
        }
    }

    /**
     * 
     * @param type $value
     * @param type $customer
     * @return type
     */
    public function getPointRule() {

        $query = new DbQuery();
        $query->select("id_mastermama_points_rule");
        $query->from("mastermama_points_rule");
        $query->where("id_country = " . (int) $this->context->country->id);
        return Db::getInstance()->getValue($query);
    }

}

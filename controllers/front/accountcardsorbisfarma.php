<?php

/**
 *  2019-2020 FARMALISTO
 *
 *  @author    Jhonny Romero <jromeroarenales@gmail.com>
 *  @copyright 2017-2020 FARMALISTO
 *  @license   https://www.farmalisto.com.mx/ - prestashop module orbisfarma
 */
require_once(_PS_MODULE_DIR_ . 'orbisfarma/classes/OrbisfarmaForm.php');
require_once(_PS_MODULE_DIR_ . 'orbisfarma/classes/OrbisfarmaFormatter.php');
require_once(_PS_MODULE_DIR_ . 'orbisfarma/orbisfarma.php');

class OrbisfarmaAccountCardsOrbisfarmaModuleFrontController extends ModuleFrontController {

    private $card_form;
    private $should_redirect = false;
    private $id_orbisfarma_service;
    private $plan;

    /**
     * 
     */
    public function __construct() {
        if (Tools::isSubmit('id')) {
            $this->id_orbisfarma_service = (int) Tools::getValue('id');
            $this->plan = orbisfarma::getPlanById($this->id_orbisfarma_service);
        }
        parent::__construct();
    }

    /**
     * Initialize address controller
     * @see FrontController::init()
     */
    public function init() {
        if (!$this->context->customer->isLogged()) {
            Tools::redirect('index.php?controller=authentication');
            exit;
        }
        parent::init();
        $this->card_form = $this->makeCardForm();
        $this->context->smarty->assign('card_form', $this->card_form->getProxy());
    }

    /**
     * Start forms process
     * @see FrontController::postProcess()
     */
    public function postProcess() {

        $this->context->smarty->assign('editing', false);
        $this->card_form->fillWith(Tools::getAllValues());

        $card_customer = OrbisfarmaForm::getCardNumber($this->id_orbisfarma_service);

        if (Tools::isSubmit('submitCard') && Tools::isSubmit('number')) {
            $card = Tools::getValue('number');
            $card_exits = OrbisfarmaForm::existCardNumber($card);
            if (!$card_exits) {
                if (!$this->card_form->submit()) {
                    if (!empty($card_customer)) {
                        $this->context->smarty->assign('editing', true);
                        $this->errors[] = "Ocurrio un error al actualizar su tarjeta, esta tarjeta ya esta en uso intentelo de nuevo";
                    } else {
                        $this->errors[] = "Ocurrio un error al agregar su tarjeta, esta tarjeta ya esta en uso intentelo de nuevo";
                    }
                } else {
                    if (Tools::isSubmit('number') && Tools::getValue('number') == "") {
                        $this->errors[] = 'Su tarjeta se eliminó correctamente!';
                    } else {
                        $this->success[] = 'Su tarjeta se actualizó correctamente!';
                    }
                    $this->should_redirect = true;
                    $this->context->smarty->assign('editing', true);
                }
            } else {
                $this->errors[] = "Ocurrio un error, esta tarjeta ya esta en uso intentelo de nuevo";
            }
        } elseif (!empty($card_customer)) {
            $this->card_form->loadCardById(OrbisfarmaForm::getCardNumber($this->id_orbisfarma_service));
            $this->context->smarty->assign('editing', true);
        }
    }

    /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent() {

        parent::initContent();

        $this->context->smarty->assign(array(
            'url' => Context::getContext()->link->getModuleLink('orbisfarma', 'account-cards-orbisfarma', ['id' => $this->id_orbisfarma_service]),
            'id_customer' => (int) Context::getContext()->customer->id,
            'id_orbisfarma_service' => $this->id_orbisfarma_service
        ));

        $this->setTemplate('module:orbisfarma/views/templates/front/orbisfarma.tpl');
    }

    public function getBreadcrumbLinks() {
        $breadcrumb = parent::getBreadcrumbLinks();

        $breadcrumb['links'][] = $this->addMyAccountToBreadcrumb();

        $breadcrumb['links'][] = [
            'title' => $this->trans($this->plan['description'], array(), 'Modules.orbisfarma.Shop'),
            'url' => Context::getContext()->link->getModuleLink('orbisfarma', 'account-cards-orbisfarma', ['id' => $this->id_orbisfarma_service])
        ];

        return $breadcrumb;
    }

    protected function makeCardForm() {

        $form = new OrbisfarmaForm(
                $this->context->smarty, $this->context, $this->getTranslator(), new OrbisfarmaFormatter(
                        $this->getTranslator(), $this->context->language, $this->module
                ), $this->id_orbisfarma_service
        );

        $form->setAction($this->getCurrentURL());
        return $form;
    }

    public function setMedia() {
        parent::setMedia();

        $this->context->controller->registerJavascript(
                'module-lms-lib',
                'modules/' . $this->module->name . '/views/js/' . $this->module->name . '-lib.js',
                [
                    'position' => 'bottom',
                    'priority' => 422,
                    'attributes' => 'async']
        );
    }

}

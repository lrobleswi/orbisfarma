<?php

/*
 *  2019-2020 FARMALISTO
 *
 *  @author    Jhonny Romero <jromeroarenales@gmail.com>
 *  @copyright 2017-2020 FARMALISTO
 *  @license   https://www.farmalisto.com.mx/ - prestashop module orbisfarma
 */

class OrbisfarmaAjaxModuleFrontController extends ModuleFrontController {

    public $ssl = true;

    public function __construct() {
        parent::__construct();
        $this->ajax = true;
    }

    /**
     * @see FrontController::initContent()
     */
    public function initContent() {

        parent::initContent();

        if (Tools::getValue('action') === 'get-footer') {
            $footer = $this->module->hookDisplayShoppingCartFooter(
                    array(
                        'cart' => $this->context->cart,
                        'footer_ajax' => 1
            ));
            ob_end_clean();
            header('Content-Type: application/json');
            die(json_encode([
                'footer' => $footer,
            ]));
        }
    }

    public function displayAjaxAddCard() {

        if ($this->isTokenValid() && Tools::isSubmit('card_number') && Tools::isSubmit('plan') && Tools::isSubmit('id_service') && Tools::isSubmit('url') && Tools::isSubmit('redirect')) {

            $this->ajax = true;
            $url = Tools::getValue('url');
            $plan = Tools::getValue('plan');
            $redirect = boolval((int) Tools::getValue('redirect'));

            if ($this->context->customer->isLogged()) {

                if ($plan == "orbisfarma") {
                    require_once _PS_MODULE_DIR_ . 'orbisfarma/classes/OrbisfarmaCustomerCard.php';
                    $customerCard = new OrbisfarmaCustomerCard();
                    $customerCard->id_orbisfarma_service = (int) Tools::getValue('id_service');
                    $customerCard->number = Tools::getValue('card_number');
                    $cardExist = $this->module->cartExists($customerCard->number);
                } elseif ($plan == "lms") {
                    require_once _PS_MODULE_DIR_ . 'lms/classes/LmsCustomerCard.php';
                    $customerCard = new LmsCustomerCard();
                    $customerCard->id_lms_service = (int) Tools::getValue('id_service');
                    $customerCard->number = Tools::getValue('card_number');
                    $lms = Module::getInstanceByName('lms');
                    $cardExist = $lms->cartExists($customerCard->number);
                } else
                    return;
                $customerCard->id_customer = (int) $this->context->customer->id;


                if (($error = $customerCard->validateFields(false, true)) !== true) {
                    $response = $error;
                    $save = false;
                    $logged = true;
                    $redirect = false;
                } elseif ($cardExist) {
                    $response = $this->module->l("Tu tarjeta ya ha sido registrada previamente!");
                    $save = false;
                    $logged = true;
                    $redirect = false;
                } elseif ($customerCard->save()) {
                    $response = $this->module->l("Su tarjeta se agregó correctamente!");
                    $save = true;
                    $logged = true;
                    $redirect = true;
                }
            } else {
                $response = $this->module->l("Inicia Sesión o Regístrate antes de ingresar tu tarjeta!");
                $save = false;
                $logged = false;
                $url = $this->context->link->getPageLink('authentication');
                $redirect = true;
            }

            $message = $this->module->getMessage(
                    array(
                        'message' => $response
            ));

            ob_end_clean();
            header('Content-Type: application/json');
            echo json_encode([
                'message' => $message,
                'save' => $save,
                'logged' => $logged,
                'url' => $url,
                'redirect' => $redirect
            ]);
        }
    }

}
